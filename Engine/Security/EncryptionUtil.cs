﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.OpenSsl;

namespace Engine.EncrypUtil
{
    /// <summary>
    /// version
    /// 2017-4-16 - today first version
    /// 2017-4-17 - Rsa Done PKCS#8
    /// </summary>
    public class GUID
    {
        /// <summary>
        /// Genarate GUID
        /// </summary>
        /// <returns></returns>
        public static string GetGUID()
        {
            string GUID = Guid.NewGuid().ToString();
            return GUID;
        }
        /// <summary>
        /// Genarate GUID in specific length 
        /// </summary>
        /// <param name="PLength"></param>
        /// <returns></returns>
        public static string GetGUID(int PLength)
        {
            string GUID = Guid.NewGuid().ToString();
            if (PLength < 2 || PLength > GUID.Length) return "Error!! cant more then " + GUID.Length + " or less then 2";
            else
            {
                GUID = GUID.Replace("-", string.Empty);
                return GUID.Substring(0, PLength);
            }
        }
    }

    public class Md5Util
    {
        public static string MD5Hash(string POriginal)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.UTF8.GetBytes(POriginal));
                return Encoding.UTF8.GetString(result);
            }
        }
    }

    public class ShaUtil
    {
        public static string SHA1Encrypt(string POriginal)
        {
            using (SHA1 sha1 = SHA1.Create())
            {
                byte[] result = sha1.ComputeHash(Encoding.UTF8.GetBytes(POriginal));
                return Encoding.UTF8.GetString(result);
            }
        }

        public static string SHA256Encrypt(string POriginal)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] result = sha256.ComputeHash(Encoding.UTF8.GetBytes(POriginal));
                return Encoding.UTF8.GetString(result);
            }
        }

        public static string SHA512Encrypt(string POriginal)
        {
            using (SHA512 sha512 = SHA512.Create())
            {
                byte[] result = sha512.ComputeHash(Encoding.UTF8.GetBytes(POriginal));
                return Encoding.UTF8.GetString(result);
            }
        }
    }

    public class AESUtil
    {
        public static string EncryptBase64(string original, string Pkey, string Piv)
        {
            return EncryptAESBase64(original, AESIV(Pkey), AESKey(Piv));
        }

        private static string EncryptAESBase64(string original, byte[] key, byte[] iv)
        {
            var buffer = Encoding.UTF8.GetBytes(original);
            using (var aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                using (var encryptor = aes.CreateEncryptor(aes.Key, aes.IV))
                using (var resultStream = new MemoryStream())
                {
                    using (var aesStream = new CryptoStream(resultStream, encryptor, CryptoStreamMode.Write))
                    using (var plainStream = new MemoryStream(buffer))
                    {
                        plainStream.CopyTo(aesStream);
                    }
                    return Convert.ToBase64String(resultStream.ToArray());
                }
            }
        }

        public static string DecryptBase64(string original, string key, string iv)
        {
            return DecryptAESBase64(original, AESKey(key), AESIV(iv));
        }
        private static string DecryptAESBase64(string base64String, byte[] key, byte[] iv)
        {
            string decrypted = "";
            using (var aes = Aes.Create())
            {

                aes.Key = key;
                aes.IV = iv;

                // create a decryptor to perform the stream transform.
                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                // create the streams used for encryption.
                var encrypted_bytes = Convert.FromBase64String(base64String);
                using (var memory_stream = new MemoryStream(encrypted_bytes))
                {
                    using (var crypto_stream = new CryptoStream(memory_stream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var reader = new StreamReader(crypto_stream))
                        {
                            decrypted = reader.ReadToEnd();
                        }
                    }
                }
            }

            return decrypted;
        }
        private static byte[] AESIV(string data)
        {
            using (var Hasher = MD5.Create())
            {
                Byte[] hashedDataBytes = Hasher.ComputeHash(Encoding.UTF8.GetBytes(data));
                return hashedDataBytes;
            }
        }
        private static byte[] AESKey(string data)
        {
            using (var Hasher = MD5.Create())
            {
                byte[] hashedDataBytes = Hasher.ComputeHash(Encoding.UTF8.GetBytes(data));
                return hashedDataBytes;
            }
        }
    }

    public class RsaUtil
    {
        //http://blog.csdn.net/gzy11/article/details/54573973#
        public static string EncryptRsaMd5(string plainText, string pemPrivateKey)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var privateKey = PemToXml("-----BEGIN PRIVATE KEY-----\r\n" + pemPrivateKey + "\r\n-----END PRIVATE KEY-----");
            RSA rsa = RSA.Create(); rsa.FromXmlString(privateKey);
            RSAParameters Key = rsa.ExportParameters(true);
            byte[] signData = rsa.SignData(plainTextBytes, HashAlgorithmName.MD5, RSASignaturePadding.Pkcs1); // pkcs8 / md5          
            return Convert.ToBase64String(signData);
        }

        public static string DecryptRsaMd5(string base64Text, string pemPrivateKey)
        {
            var cipherBytes = Convert.FromBase64String(base64Text);
            var privateKey = PemToXml(pemPrivateKey);
            RSA rsa = RSA.Create();
            rsa.FromXmlString(privateKey);
            byte[] signHash = rsa.Decrypt(cipherBytes, RSAEncryptionPadding.CreateOaep(HashAlgorithmName.MD5));
            var result = Encoding.UTF8.GetString(signHash);
            return result;
        }

        public static string EncryptRsaSha1(string plainText, string pemPrivateKey)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var privateKey = PemToXml(pemPrivateKey);
            RSA rsa = RSA.Create(); rsa.FromXmlString(privateKey);
            RSAParameters Key = rsa.ExportParameters(true);
            byte[] signData = rsa.Encrypt(plainTextBytes, RSAEncryptionPadding.OaepSHA1);
            return Convert.ToBase64String(signData);
        }

        public static string DecryptRsaSha1(string base64Text, string pemPrivateKey)
        {
            var cipherBytes = Convert.FromBase64String(base64Text);
            var privateKey = PemToXml(pemPrivateKey);
            RSA rsa = RSA.Create();
            rsa.FromXmlString(privateKey);
            byte[] signHash = rsa.Decrypt(cipherBytes, RSAEncryptionPadding.OaepSHA1);
            var result = Encoding.UTF8.GetString(signHash);
            return result;
        }

        private static string PemToXml(string pem)
        {
            if (pem.StartsWith("-----BEGIN RSA PRIVATE KEY-----") || pem.StartsWith("-----BEGIN PRIVATE KEY-----"))
            {
                return GetXmlRsaKey(pem, obj =>
                {
                    if ((obj as RsaPrivateCrtKeyParameters) != null)
                        return DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)obj);
                    var keyPair = (AsymmetricCipherKeyPair)obj;
                    return DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)keyPair.Private);
                }, rsa => rsa.ToXmlString(true));
            }

            if (pem.StartsWith("-----BEGIN PUBLIC KEY-----"))
            {
                return GetXmlRsaKey(pem, obj =>
                {
                    var publicKey = (RsaKeyParameters)obj;
                    return DotNetUtilities.ToRSA(publicKey);
                }, rsa => rsa.ToXmlString(false));
            }

            throw new InvalidKeyException("Unsupported PEM format...");
        }
        private static string GetXmlRsaKey(string pem, Func<object, RSA> getRsa, Func<RSA, string> getKey)
        {
            using (var ms = new MemoryStream())
            using (var sw = new StreamWriter(ms))
            using (var sr = new StreamReader(ms))
            {
                sw.Write(pem);
                sw.Flush();
                ms.Position = 0;
                var pr = new PemReader(sr);
                object keyPair = pr.ReadObject();
                using (RSA rsa = getRsa(keyPair))
                {
                    var xml = getKey(rsa);
                    return xml;
                }
            }
        }
    }

}
