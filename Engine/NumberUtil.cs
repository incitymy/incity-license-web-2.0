﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Engine
{
    public class NumberUtil
    {
        public static decimal Round(decimal value, int decimalPlace)
        {
            return Math.Round(value, decimalPlace, MidpointRounding.AwayFromZero);
        }

        public static string ToDecimalPlace(decimal value, int decimalPlace)
        {
            return value.ToString(string.Format("N{0}", decimalPlace.ToString()));
        }

        public static int ConvertInteger(string value)
        {
            return Int32.Parse(value);
        }

    }
}
