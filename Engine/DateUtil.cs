﻿using System;

namespace Engine
{
    public class DateUtil
    {
        public static DateTime GetFixDate(int day, int month, int year, bool isStart)
        {
            if (isStart)
                return new DateTime(year, month, day, 0, 0, 0, 0);
            else
                return new DateTime(year, month, day, 23, 59, 59, 999);
        }
        public static DateTime GetFixDate(DateTime date, bool isStart)
        {
            if (isStart)
                return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
            else
                return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }
        public static DateTime GetTodayDate(bool isStart)
        {
            if (isStart)
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, 0);
            else
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
        }
        public static DateTime GetMonthlyDate(int month, int year, bool isStart)
        {
            if (isStart)
                return new DateTime(year, month, 1, 0, 0, 0, 0);
            else
            {
                DateTime lastDay = new DateTime(year, month, DateTime.Now.Day).AddMonths(1).AddDays(-1);

                return new DateTime(lastDay.Year, lastDay.Month, lastDay.Day, 23, 59, 59, 999);
            }
        }
        public static DateTime GetWeeklyDate(bool isStart)
        {
            if (isStart)
            {
                DateTime date = DateTime.Now.AddDays(GetDistanceFirstDay(DateTime.Now.DayOfWeek));
                return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
            }                
            else
            {
                DateTime date = DateTime.Now.AddDays(GetDistanceLastDay(DateTime.Now.DayOfWeek));
                return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
            }
        }
        public static int GetDistanceFirstDay(DayOfWeek day)
        {
            switch(day)
            {
                case DayOfWeek.Sunday:
                    return -6;
                case DayOfWeek.Saturday:
                    return -5;
                case DayOfWeek.Friday:
                    return -4;
                case DayOfWeek.Thursday:
                    return -3;
                case DayOfWeek.Wednesday:
                    return -2;
                case DayOfWeek.Tuesday:
                    return -1;
                case DayOfWeek.Monday:
                    return -0;
                default:
                    return -7;
            }
        }
        public static int GetDistanceLastDay(DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Sunday:
                    return -2;
                case DayOfWeek.Saturday:
                    return -1;
                case DayOfWeek.Friday:
                    return 0;
                case DayOfWeek.Thursday:
                    return 1;
                case DayOfWeek.Wednesday:
                    return 2;
                case DayOfWeek.Tuesday:
                    return 3;
                case DayOfWeek.Monday:
                    return 4;
                default:
                    return 5;
            }
        }
        public static string ToShortDate(DateTime? datetime)
        {
            string shortDate = "";
            if(datetime!=null)
            {
                if (((DateTime)datetime).Year == 1)
                    shortDate = "";
                else
                    shortDate = ((DateTime)datetime).ToString("dd MMM yyyy");                
            }

            return shortDate;
        }

        public static string ToLongDate(DateTime dateTime)
        {
            return dateTime.ToString(GetLongDateFormat());
        }
        public static string ToLongDateTime(DateTime dateTime)
        {
            return dateTime.ToString(GetLongDateTimeFormat());
        }

        public static string GetLongDateFormat()
        {
            return "MM/dd/yyyy";
        }
        public static string GetLongDateTimeFormat()
        {
            return "MM/dd/yyyy HH:mm:ss";
        }
        public static DateTime ConvertDate(string date)
        {
            string[] dateArr = date.Split('/');
            DateTime newDate = new DateTime(NumberUtil.ConvertInteger(dateArr[2]), 
                                            NumberUtil.ConvertInteger(dateArr[0]), 
                                            NumberUtil.ConvertInteger(dateArr[1]));
            return newDate;
        }
    }
}
