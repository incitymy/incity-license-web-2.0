﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Engine
{
    public class ReportUtil
    {
        public static string GetColumn(int column)
        {
            string columnHtml = "<tr>";
            for (int i = 0; i < column; i++)
            {
                columnHtml += "<td>{" + i + "}</td>";
            }
            columnHtml += "</tr>";
            return columnHtml;
        }

    }
}
