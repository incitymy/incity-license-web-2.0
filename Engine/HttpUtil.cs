﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Engine
{
    public class HttpUtil
    {
        public async Task<string> SendPostAsync(string url, string jsonData)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(url)
            };
            var response = await client.PostAsync(url, new StringContent(jsonData, Encoding.UTF8, "application/json"));
            var created = await response.Content.ReadAsStringAsync();
            return created;
        }

        public async Task<string> SendGetAsync(string url)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(url)
            };
            var response = await client.GetAsync("");
            var post = await response.Content.ReadAsStringAsync();
            return post;
        }

        public static string GetIPAddress(HttpContext httpContext)
        {
            return httpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}
