﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;

namespace IncityApp.BLL
{
    public class LicenseService : ILicenseService
    {
        private string pageName = "License";
        private string selfName = "IncityApp.BLL.LicenseService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private ILicenseRepository licenseRepo;
        private ILicenseCategoryRepository licenseCategoryRepo;

        public LicenseService(ILicenseRepository _licenseRepo, ILicenseCategoryRepository _licenseCategoryRepo)
        {
            licenseRepo = _licenseRepo;
            licenseCategoryRepo = _licenseCategoryRepo;
        }

        public License GetActive(int id, out string ErrMsg)
        {
            License result = new License();

            try
            {
                result = licenseRepo.GetActive(id, out ErrMsg);
                result.LicenseCategory = licenseCategoryRepo.GetActive(result.LicenseCategoryId, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<License> GetActiveList(out string ErrMsg)
        {
            List<License> list = new List<License>();
            try
            {
                list = licenseRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<License> GetDeletedList(out string ErrMsg)
        {
            List<License> list = new List<License>();
            try
            {
                list = licenseRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(License license, out string ErrMsg)
        {
            try
            {
                licenseRepo.IsStringExists(license.Name, license.LicenseCategoryId, false, out ErrMsg);

                if (ErrMsg == "")
                {
                    licenseRepo.Create(license, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(License license, out string ErrMsg)
        {
            try
            {
                licenseRepo.IsStringExists(license.Name, license.LicenseCategoryId, true, out ErrMsg);

                if (ErrMsg == "")
                {
                    license.LastUpdate = DateTime.Now;
                    license.LastUser = "Admin";
                    licenseRepo.Edit(license, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                licenseRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                licenseRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                licenseRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
