﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface ILicenseService
    {
        License GetActive(int id, out string ErrMsg);
        List<License> GetActiveList(out string ErrMsg);
        List<License> GetDeletedList(out string ErrMsg);
        void Create(License license, out string ErrMsg);
        void Edit(License license, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
    }
}
