﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface IOrderDetailService
    {
        OrderDetail GetActive(int id, out string ErrMsg);
        List<OrderDetail> GetActiveList(out string ErrMsg);
        List<OrderDetail> GetDeletedList(out string ErrMsg);
        void Create(OrderDetail license, out string ErrMsg);
        void Edit(OrderDetail license, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void CheckLicenseCode(CheckLicenseModel checkLicenseModel, out ResponseModel response);
    }
}
