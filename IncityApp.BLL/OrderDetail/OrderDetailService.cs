﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;

namespace IncityApp.BLL
{
    public class OrderDetailService : IOrderDetailService
    {
        private string pageName = "OrderDetail ";
        private string selfName = "IncityApp.BLL.OrderDetailService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IOrderDetailRepository orderDetailRepo;
        private IOrderRepository orderRepo;

        public OrderDetailService(IOrderDetailRepository _orderDetailRepo, IOrderRepository _orderRepo)
        {
            orderDetailRepo = _orderDetailRepo;
            orderRepo = _orderRepo;
        }

        public OrderDetail GetActive(int id, out string ErrMsg)
        {
            OrderDetail result = new OrderDetail();

            try
            {
                result = orderDetailRepo.GetActive(id, out ErrMsg);
                result.Order = orderRepo.GetActive(result.OrderId, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<OrderDetail> GetActiveList(out string ErrMsg)
        {
            List<OrderDetail> list = new List<OrderDetail>();
            try
            {
                list = orderDetailRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<OrderDetail> GetDeletedList(out string ErrMsg)
        {
            List<OrderDetail> list = new List<OrderDetail>();
            try
            {
                list = orderDetailRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(OrderDetail orderDetail, out string ErrMsg)
        {
            try
            {
                //orderDetailRepo.IsStringExists(orderDetail.Name, orderDetail.OrderId, false, out ErrMsg);

                //if (ErrMsg == "")
                //{
                orderDetailRepo.Create(orderDetail, out ErrMsg);
                //}
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            
        }

        public void Edit(OrderDetail orderDetail, out string ErrMsg)
        {
            try
            {
                //orderDetailRepo.IsStringExists(orderDetail.Name, orderDetail.OrderId, true, out ErrMsg);

                //if (ErrMsg == "")
                //{
                orderDetail.LastUpdate = DateTime.Now;
                orderDetail.LastUser = "Member";
                orderDetailRepo.Edit(orderDetail, out ErrMsg);
                //}
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                orderDetailRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                orderDetailRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                orderDetailRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            
        }

        // API
        public void CheckLicenseCode(CheckLicenseModel checkLicenseModel, out ResponseModel response)
        {
            try
            {
                orderDetailRepo.CheckLicenseCode(checkLicenseModel, out response);
            }
            catch (Exception ex)
            {
                response = new ResponseModel()
                {
                    ResponseMessage = "An error has occured at " + selfName + "\n" + ex.Message.ToString()
                };
            }
        }
    }
}
