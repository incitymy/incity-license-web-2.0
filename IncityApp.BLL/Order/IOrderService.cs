﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface IOrderService
    {
        Order GetActive(int id, out string ErrMsg);
        List<Order> GetActiveList(out string ErrMsg);
        List<Order> GetDeletedList(out string ErrMsg);
        void Create(CheckoutModel checkoutModel, out string ErrMsg);
        void Edit(Order order, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
    }
}
