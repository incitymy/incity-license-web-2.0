﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;

namespace IncityApp.BLL
{
    public class OrderService : IOrderService
    {
        private string pageName = "Order";
        private string selfName = "IncityApp.BLL.OrderService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IOrderRepository orderRepo;
        private IOrderDetailRepository orderDetailRepo;

        public OrderService(IOrderRepository _orderRepo, IOrderDetailRepository _orderDetailRepo)
        {
            orderRepo = _orderRepo;
            orderDetailRepo = _orderDetailRepo;
        }

        public Order GetActive(int id, out string ErrMsg)
        {
            Order result = new Order();

            try
            {
                result = orderRepo.GetActive(id, out ErrMsg);
            }
            catch(Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<Order> GetActiveList(out string ErrMsg)
        {
            List<Order> list = new List<Order>();
            try
            {
                list = orderRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<Order> GetDeletedList(out string ErrMsg)
        {
            List<Order> list = new List<Order>();
            try
            {
                list = orderRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(CheckoutModel checkoutModel, out string ErrMsg)
        {
            try
            {
                Order order = new Order();
                Order lastOrder = orderRepo.GetAll().OrderByDescending(p => p.CreateDate).FirstOrDefault();

                if (lastOrder == null)
                {
                    lastOrder = new Order();
                    lastOrder.Id = 0;
                }

                int startNo = 1000000;
                int refNo = startNo + lastOrder.Id + 1;
                order.RefNo = "Ref" + refNo;

                order.MemberId = checkoutModel.member.Id;
                order.GrandTotal = checkoutModel.cart.grandTotal;
                order.PaymentMethod = checkoutModel.PaymentMethod;
                order.Status = "Pending";
                order.IsActive = false;
                order.IsDeleted = false;
                order.CreateBy = "Member Id: " + order.MemberId.ToString();
                order.CreateDate = DateTime.Now;
                order.LastUser = "Member Id: " + order.MemberId.ToString();
                order.LastUpdate = DateTime.Now;
                order.Remark = "";

                orderRepo.Create(order, out ErrMsg);

                if (ErrMsg == "")
                {
                    foreach (CartItem item in checkoutModel.cart.cartItems)
                    {
                        order = orderRepo.GetAll().Last();
                        OrderDetail orderDetail = new OrderDetail()
                        {
                            LicenseCode = NivlekApp.Niv_Secure.GetGUID(12),
                            OrderId = order.Id,
                            LicenseId = item.license.Id,
                            Qty = item.qty,
                            SubTotal = item.subtotal,
                            IsActive = false,
                            IsDeleted = false,
                            CreateBy = "Member Id: " + order.MemberId.ToString(),
                            CreateDate = DateTime.Now,
                            LastUser = "Member Id: " + order.MemberId.ToString(),
                            LastUpdate = DateTime.Now,
                            Remark = "",
                            PeriodDay = item.license.PeriodDay
                        };

                        orderDetailRepo.Create(orderDetail, out ErrMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(Order order, out string ErrMsg)
        {
            try
            {
                //orderRepo.IsStringExists(order.RefNo, true, out ErrMsg);

                //if (ErrMsg == "")
                //{
                order.LastUpdate = DateTime.Now;
                //order.LastUser = "Admin";
                order.LastUser = "MemberId " + order.MemberId.ToString();
                orderRepo.Edit(order, out ErrMsg);
                //}
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                orderRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                orderRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                orderRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
