﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public class AdminLogService : IAdminLogService
    {
        private string selfName = "IncityApp.BLL.AdminLogService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IAdminLogRepository repository;

        public AdminLogService(IAdminLogRepository _repository)
        {
            repository = _repository;
        }

        public List<AdminLog> GetActiveList(out string ErrMsg)
        {
            List<AdminLog> list = new List<AdminLog>();
            try
            {
                list = repository.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return list;
        }

        public void Create(string action, string actionPosition, string ipAddress, string remark, out string ErrMsg)
        {
            try
            {
                AdminLog userLog = new AdminLog();
                userLog.Action = action;
                userLog.ActionPosition = actionPosition;
                userLog.IpAddress = ipAddress;
                userLog.Remark = remark;
                userLog.IsActive = true; // Comment this line if user can select active status.
                userLog.IsDeleted = false;
                userLog.CreateDate = DateTime.Now;
                userLog.CreateBy = "Admin";
                userLog.LastUpdate = DateTime.Now;
                userLog.LastUser = "Admin";

                repository.Create(userLog, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return;
        }
    }
}
