﻿using System.Collections.Generic;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface IAdminLogService
    {
        List<AdminLog> GetActiveList(out string ErrMsg);
        void Create(string action, string actionPosition, string ipAddress, string remark, out string ErrMsg);
    }
}
