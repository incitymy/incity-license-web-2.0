﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace IncityApp.BLL
{
    public class MemberService : IMemberService
    {
        private string pageName = "Member ";
        private string selfName = "IncityApp.BLL.MemberService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IMemberRepository memberRepo;

        public MemberService(IMemberRepository _memberRepo)
        {
            memberRepo = _memberRepo;
        }

        public string EncryptString(string text, string keyString)
        {
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public string DecryptString(string cipherText, string keyString)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }

        public Member GetMember(string email, string password, string encryptKey, out string ErrMsg)
        {
            Member member = new Member();
            try
            {
                //if (memberRepo.VerifyLogin(username, password, out ErrMsg))
                //{
                //    member = memberRepo.GetByCondition(p => p.Username == username).FirstOrDefault();
                //}
                member = memberRepo.GetByCondition(p => p.Email == email).FirstOrDefault();

                if(DecryptString(member.Password, encryptKey) == password)
                {
                    ErrMsg = "";
                }
                else
                {
                    ErrMsg = "Login failed. Incorrect email or password.";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return member;
        }

        public Member GetActive(int id, out string ErrMsg)
        {
            Member result = new Member();

            try
            {
                result = memberRepo.GetActive(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<Member> GetActiveList(out string ErrMsg)
        {
            List<Member> list = new List<Member>();
            try
            {
                list = memberRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<Member> GetDeletedList(out string ErrMsg)
        {
            List<Member> list = new List<Member>();
            try
            {
                list = memberRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(Member member, out string ErrMsg)
        {
            try
            {
                memberRepo.IsStringExists(member.Email, false, out ErrMsg);

                if (ErrMsg == "")
                {
                    memberRepo.Create(member, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(Member member, out string ErrMsg)
        {
            try
            {
                memberRepo.IsStringExists(member.Email, true, out ErrMsg);

                if (ErrMsg == "")
                {
                    member.LastUpdate = DateTime.Now;
                    member.LastUser = "Member";
                    memberRepo.Edit(member, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                memberRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                memberRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                memberRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
