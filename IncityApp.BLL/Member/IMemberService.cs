﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface IMemberService
    {
        string EncryptString(string text, string keyString);
        string DecryptString(string cipherText, string keyString);
        Member GetMember(string username, string password, string encryptKey, out string ErrMsg);
        Member GetActive(int id, out string ErrMsg);
        List<Member> GetActiveList(out string ErrMsg);
        List<Member> GetDeletedList(out string ErrMsg);
        void Create(Member member, out string ErrMsg);
        void Edit(Member member, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
    }
}
