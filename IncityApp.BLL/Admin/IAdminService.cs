﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface IAdminService
    {
        string EncryptString(string text, string keyString);
        string DecryptString(string cipherText, string keyString);
        Admin GetAdmin(string username, string password, string encryptKey, out string ErrMsg);
        Admin GetActive(int id, out string ErrMsg);
        List<Admin> GetActiveList(out string ErrMsg);
        List<Admin> GetDeletedList(out string ErrMsg);
        void Create(Admin admin, out string ErrMsg);
        void Edit(Admin admin, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
    }
}
