﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace IncityApp.BLL
{
    public class AdminService : IAdminService
    {
        private string pageName = "Admin ";
        private string selfName = "IncityApp.BLL.AdminService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IAdminRepository adminRepo;
        private IAdminGroupRepository adminGroupRepo;

        public AdminService(IAdminRepository _adminRepo, IAdminGroupRepository _adminGroupRepo)
        {
            adminRepo = _adminRepo;
            adminGroupRepo = _adminGroupRepo;
        }

        public string EncryptString(string text, string keyString)
        {
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public string DecryptString(string cipherText, string keyString)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }

        public Admin GetAdmin(string username, string password, string encryptKey, out string ErrMsg)
        {
            Admin admin = new Admin();
            try
            {
                admin = adminRepo.GetByCondition(p => p.Username == username).FirstOrDefault();
                admin.AdminGroup = adminGroupRepo.GetByCondition(p => p.Id == admin.AdminGroupId).FirstOrDefault();

                if(DecryptString(admin.Password, encryptKey) == password)
                {
                    ErrMsg = "";
                }
                else
                {
                    ErrMsg = "Login failed. Incorrect username or password.";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return admin;
        }

        public Admin GetActive(int id, out string ErrMsg)
        {
            Admin result = new Admin();

            try
            {
                result = adminRepo.GetActive(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<Admin> GetActiveList(out string ErrMsg)
        {
            List<Admin> list = new List<Admin>();
            try
            {
                list = adminRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<Admin> GetDeletedList(out string ErrMsg)
        {
            List<Admin> list = new List<Admin>();
            try
            {
                list = adminRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(Admin admin, out string ErrMsg)
        {
            try
            {
                adminRepo.IsStringExists(admin.Username, false, out ErrMsg);

                if (ErrMsg == "")
                {
                    adminRepo.Create(admin, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(Admin admin, out string ErrMsg)
        {
            try
            {
                adminRepo.IsStringExists(admin.Username, true, out ErrMsg);

                if (ErrMsg == "")
                {
                    admin.LastUpdate = DateTime.Now;
                    admin.LastUser = "Admin";
                    adminRepo.Edit(admin, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                adminRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                adminRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                adminRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
