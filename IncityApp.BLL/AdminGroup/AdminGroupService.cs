﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;

namespace IncityApp.BLL
{
    public class AdminGroupService : IAdminGroupService
    {
        private string pageName = "AdminGroup ";
        private string selfName = "IncityApp.BLL.AdminGroupService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private IAdminGroupRepository adminGroupRepo;

        public AdminGroupService(IAdminGroupRepository _adminGroupRepo)
        {
            adminGroupRepo = _adminGroupRepo;
        }

        public AdminGroup GetActive(int id, out string ErrMsg)
        {
            AdminGroup result = new AdminGroup();

            try
            {
                result = adminGroupRepo.GetActive(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<AdminGroup> GetActiveList(out string ErrMsg)
        {
            List<AdminGroup> list = new List<AdminGroup>();
            try
            {
                list = adminGroupRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<AdminGroup> GetDeletedList(out string ErrMsg)
        {
            List<AdminGroup> list = new List<AdminGroup>();
            try
            {
                list = adminGroupRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(AdminGroup adminGroup, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.IsStringExists(adminGroup.Name, false, out ErrMsg);

                if (ErrMsg == "")
                {
                    adminGroupRepo.Create(adminGroup, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(AdminGroup adminGroup, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.IsStringExists(adminGroup.Name, true, out ErrMsg);

                if (ErrMsg == "")
                {
                    adminGroup.LastUpdate = DateTime.Now;
                    adminGroup.LastUser = "Admin";
                    adminGroupRepo.Edit(adminGroup, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
