﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.BLL
{
    public interface ILicenseCategoryService
    {
        LicenseCategory GetActive(int id, out string ErrMsg);
        List<LicenseCategory> GetActiveList(out string ErrMsg);
        List<LicenseCategory> GetDeletedList(out string ErrMsg);
        void Create(LicenseCategory licenseCategory, out string ErrMsg);
        void Edit(LicenseCategory licenseCategory, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
    }
}
