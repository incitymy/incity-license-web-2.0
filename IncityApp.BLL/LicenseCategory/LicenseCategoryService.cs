﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.DAL.Repository;
using IncityApp.Models;
using Engine;
using Microsoft.AspNetCore.Http;

namespace IncityApp.BLL
{
    public class LicenseCategoryService : ILicenseCategoryService
    {
        private string pageName = "License Category";
        private string selfName = "IncityApp.BLL.LicenseCategoryService"; // This class's name, uses for ErrMsg to identifying which class has the error.

        private ILicenseCategoryRepository licenseCategoryRepo;

        public LicenseCategoryService(ILicenseCategoryRepository _licenseCategoryRepo)
        {
            licenseCategoryRepo = _licenseCategoryRepo;
        }

        public LicenseCategory GetActive(int id, out string ErrMsg)
        {
            LicenseCategory result = new LicenseCategory();

            try
            {
                result = licenseCategoryRepo.GetActive(id, out ErrMsg);
            }
            catch(Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }

            return result;
        }

        public List<LicenseCategory> GetActiveList(out string ErrMsg)
        {
            List<LicenseCategory> list = new List<LicenseCategory>();
            try
            {
                list = licenseCategoryRepo.GetActiveList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public List<LicenseCategory> GetDeletedList(out string ErrMsg)
        {
            List<LicenseCategory> list = new List<LicenseCategory>();
            try
            {
                list = licenseCategoryRepo.GetDeletedList(out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return list;
        }

        public void Create(LicenseCategory licenseCategory, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.IsStringExists(licenseCategory.Name, false, out ErrMsg);

                if (ErrMsg == "")
                {
                    licenseCategoryRepo.Create(licenseCategory, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Edit(LicenseCategory licenseCategory, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.IsStringExists(licenseCategory.Name, true, out ErrMsg);

                if (ErrMsg == "")
                {
                    licenseCategory.LastUpdate = DateTime.Now;
                    licenseCategory.LastUser = "Admin";
                    licenseCategoryRepo.Edit(licenseCategory, out ErrMsg);
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.Delete(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.DeletePermanant(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.Restore(id, out ErrMsg);
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "<br/>" + ex.ToString();
            }
            return;
        }
    }
}
