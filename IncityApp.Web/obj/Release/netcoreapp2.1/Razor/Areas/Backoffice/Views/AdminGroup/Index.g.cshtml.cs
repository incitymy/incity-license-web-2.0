#pragma checksum "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e630e319466e77431b39c300019d4d30c483b548"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Backoffice_Views_AdminGroup_Index), @"mvc.1.0.view", @"/Areas/Backoffice/Views/AdminGroup/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Backoffice/Views/AdminGroup/Index.cshtml", typeof(AspNetCore.Areas_Backoffice_Views_AdminGroup_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using IncityApp;

#line default
#line hidden
#line 2 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using IncityApp.Models;

#line default
#line hidden
#line 3 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e630e319466e77431b39c300019d4d30c483b548", @"/Areas/Backoffice/Views/AdminGroup/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"18814b07b6ffd99f73fb96be6653ff88c819215b", @"/Areas/Backoffice/Views/_ViewImports.cshtml")]
    public class Areas_Backoffice_Views_AdminGroup_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IncityApp.Models.AdminGroup>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/dataTables/datatables.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("names", "Development,Staging,Production", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/dataTables/datatables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(36, 106, true);
            WriteLiteral("\r\n<div class=\"row wrapper border-bottom white-bg page-heading\">\r\n    <div class=\"col-lg-10\">\r\n        <h2>");
            EndContext();
            BeginContext(143, 17, false);
#line 5 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
       Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(160, 76, true);
            WriteLiteral("</h2>\r\n        <ol class=\"breadcrumb\">\r\n            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 236, "\"", 276, 1);
#line 8 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 243, Url.Action("Dashboard", "Index"), 243, 33, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(277, 66, true);
            WriteLiteral(">Home</a>\r\n            </li>\r\n            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 343, "\"", 384, 1);
#line 11 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 350, Url.Action("Index", "AdminGroup"), 350, 34, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(385, 94, true);
            WriteLiteral(">AdminGroup </a>\r\n            </li>\r\n            <li class=\"active\">\r\n                <strong>");
            EndContext();
            BeginContext(480, 17, false);
#line 14 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                   Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(497, 155, true);
            WriteLiteral("</strong>\r\n            </li>\r\n        </ol>\r\n    </div>\r\n    <div class=\"col-lg-2 right\">\r\n        <button type=\"button\" class=\"btn btn-w-m btn-primary\"><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 652, "\"", 694, 1);
#line 19 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 659, Url.Action("Create", "AdminGroup"), 659, 35, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(695, 164, true);
            WriteLiteral(">Create New</a></button>\r\n    </div>\r\n</div>\r\n<div class=\"wrapper wrapper-content animated fadeInRight\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n\r\n");
            EndContext();
#line 26 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
             if (ViewBag.SucMsg != "")
            {

#line default
#line hidden
            BeginContext(914, 42, true);
            WriteLiteral("                <div class=\"view-success\">");
            EndContext();
            BeginContext(957, 24, false);
#line 28 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                     Write(Html.Raw(ViewBag.SucMsg));

#line default
#line hidden
            EndContext();
            BeginContext(981, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
#line 29 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(1004, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 31 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
             if (ViewBag.ErrMsg != "")
            {

#line default
#line hidden
            BeginContext(1061, 40, true);
            WriteLiteral("                <div class=\"view-error\">");
            EndContext();
            BeginContext(1102, 24, false);
#line 33 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                   Write(Html.Raw(ViewBag.ErrMsg));

#line default
#line hidden
            EndContext();
            BeginContext(1126, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
#line 34 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(1149, 302, true);
            WriteLiteral(@"
            <div class=""ibox float-e-margins"">
                <div class=""ibox-content"">

                    <table class=""table table-striped table-bordered table-hover dataTables-example"">
                        <thead>
                            <tr>
                                <th>");
            EndContext();
            BeginContext(1452, 32, false);
#line 42 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1484, 43, true);
            WriteLiteral("</th>\r\n                                <th>");
            EndContext();
            BeginContext(1528, 36, false);
#line 43 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.IsActive));

#line default
#line hidden
            EndContext();
            BeginContext(1564, 43, true);
            WriteLiteral("</th>\r\n                                <th>");
            EndContext();
            BeginContext(1608, 38, false);
#line 44 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.LastUpdate));

#line default
#line hidden
            EndContext();
            BeginContext(1646, 6, true);
            WriteLiteral("</th> ");
            EndContext();
            BeginContext(1759, 39, true);
            WriteLiteral(" \r\n                                <th>");
            EndContext();
            BeginContext(1799, 36, false);
#line 45 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.LastUser));

#line default
#line hidden
            EndContext();
            BeginContext(1835, 152, true);
            WriteLiteral("</th>\r\n                                <th></th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n");
            EndContext();
#line 50 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                             foreach (AdminGroup adminGroup in (List<AdminGroup>)ViewBag.AdminGroupList)
                            {

#line default
#line hidden
            BeginContext(2124, 70, true);
            WriteLiteral("                            <tr>\r\n                                <td>");
            EndContext();
            BeginContext(2195, 37, false);
#line 53 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayFor(p => adminGroup.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2232, 43, true);
            WriteLiteral("</td>\r\n                                <td>");
            EndContext();
            BeginContext(2276, 41, false);
#line 54 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayFor(p => adminGroup.IsActive));

#line default
#line hidden
            EndContext();
            BeginContext(2317, 43, true);
            WriteLiteral("</td>\r\n                                <td>");
            EndContext();
            BeginContext(2361, 43, false);
#line 55 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayFor(p => adminGroup.LastUpdate));

#line default
#line hidden
            EndContext();
            BeginContext(2404, 43, true);
            WriteLiteral("</td>\r\n                                <td>");
            EndContext();
            BeginContext(2448, 41, false);
#line 56 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                               Write(Html.DisplayFor(p => adminGroup.LastUser));

#line default
#line hidden
            EndContext();
            BeginContext(2489, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
#line 57 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                 if (ViewBag.IsTrash != "1")
                                {

#line default
#line hidden
            BeginContext(2593, 42, true);
            WriteLiteral("                                    <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2635, "\"", 2689, 1);
#line 59 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 2642, Url.Action("Edit", new { id = adminGroup.Id }), 2642, 47, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2690, 14, true);
            WriteLiteral(">Edit</a> | <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2704, "\"", 2760, 1);
#line 59 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 2711, Url.Action("Delete", new { id = adminGroup.Id }), 2711, 49, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2761, 18, true);
            WriteLiteral(">Delete</a></td>\r\n");
            EndContext();
#line 60 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                }
                                else
                                {

#line default
#line hidden
            BeginContext(2887, 42, true);
            WriteLiteral("                                    <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2929, "\"", 2986, 1);
#line 63 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 2936, Url.Action("Restore", new { id = adminGroup.Id }), 2936, 50, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2987, 17, true);
            WriteLiteral(">Restore</a> | <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3004, "\"", 3073, 1);
#line 63 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 3011, Url.Action("Delete", new { id = adminGroup.Id, trash = "1" }), 3011, 62, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3074, 18, true);
            WriteLiteral(">Delete</a></td>\r\n");
            EndContext();
#line 64 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                }

#line default
#line hidden
            BeginContext(3127, 35, true);
            WriteLiteral("                            </tr>\r\n");
            EndContext();
#line 66 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                            }

#line default
#line hidden
            BeginContext(3193, 66, true);
            WriteLiteral("                        </tbody>\r\n                    </table>\r\n\r\n");
            EndContext();
#line 70 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                     if (ViewBag.IsTrash != "1")
                    {

#line default
#line hidden
            BeginContext(3332, 45, true);
            WriteLiteral("                        <a class=\"trash-link\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3377, "\"", 3425, 1);
#line 72 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 3384, Url.Action("Index", new { trash = "1" }), 3384, 41, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3426, 13, true);
            WriteLiteral(">Trash List (");
            EndContext();
            BeginContext(3440, 18, false);
#line 72 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                                                                                                      Write(ViewBag.TrashCount);

#line default
#line hidden
            EndContext();
            BeginContext(3458, 7, true);
            WriteLiteral(")</a>\r\n");
            EndContext();
#line 73 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                    }
                    else
                    {

#line default
#line hidden
            BeginContext(3537, 50, true);
            WriteLiteral("                        <a class=\"trash-link back\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3587, "\"", 3614, 1);
#line 76 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
WriteAttributeValue("", 3594, Url.Action("Index"), 3594, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3615, 19, true);
            WriteLiteral(">Back To List</a>\r\n");
            EndContext();
#line 77 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\AdminGroup\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(3657, 84, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Styles", async() => {
                BeginContext(3758, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(3764, 150, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f6f1a0b2fe7e48a28999f834c5370d22", async() => {
                    BeginContext(3816, 10, true);
                    WriteLiteral("\r\n        ");
                    EndContext();
                    BeginContext(3826, 68, false);
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "94bee88884824768bbbdc2dcfb974613", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    EndContext();
                    BeginContext(3894, 6, true);
                    WriteLiteral("\r\n    ");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3914, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            BeginContext(3919, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(3939, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(3945, 140, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6965758cb0ea445a8ee340fd5a200e87", async() => {
                    BeginContext(3997, 10, true);
                    WriteLiteral("\r\n        ");
                    EndContext();
                    BeginContext(4007, 58, false);
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "65e4c5c50dcd469baac687075f5e0a22", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    EndContext();
                    BeginContext(4065, 6, true);
                    WriteLiteral("\r\n    ");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4085, 1101, true);
                WriteLiteral(@"

    <script type=""text/javascript"">
        $(document).ready(function () {

            $('.dataTables-example').DataTable({
                pageLength: 25,
                columnDefs: [{ ""type"": ""date"", ""targets"": 2 }],
                order: [[2, ""desc""]],
                dom: '<""html5buttons""B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel' },
                    { extend: 'pdf' },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
   ");
                WriteLiteral("             ]\r\n\r\n            });\r\n\r\n\r\n        });\r\n\r\n      \r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.ApplicationInsights.AspNetCore.JavaScriptSnippet JavaScriptSnippet { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IncityApp.Models.AdminGroup> Html { get; private set; }
    }
}
#pragma warning restore 1591
