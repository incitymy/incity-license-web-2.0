#pragma checksum "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d8c84c7c4c5262c254ff86c6ba3fba18a4a87856"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Backoffice_Views_LicenseCategory_Index), @"mvc.1.0.view", @"/Areas/Backoffice/Views/LicenseCategory/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Areas/Backoffice/Views/LicenseCategory/Index.cshtml", typeof(AspNetCore.Areas_Backoffice_Views_LicenseCategory_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using IncityApp;

#line default
#line hidden
#line 2 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using IncityApp.Models;

#line default
#line hidden
#line 3 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d8c84c7c4c5262c254ff86c6ba3fba18a4a87856", @"/Areas/Backoffice/Views/LicenseCategory/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"18814b07b6ffd99f73fb96be6653ff88c819215b", @"/Areas/Backoffice/Views/_ViewImports.cshtml")]
    public class Areas_Backoffice_Views_LicenseCategory_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IncityApp.Models.LicenseCategory>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/dataTables/datatables.min.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("names", "Development,Staging,Production", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/dataTables/datatables.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(41, 106, true);
            WriteLiteral("\r\n<div class=\"row wrapper border-bottom white-bg page-heading\">\r\n    <div class=\"col-lg-10\">\r\n        <h2>");
            EndContext();
            BeginContext(148, 17, false);
#line 5 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
       Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(165, 76, true);
            WriteLiteral("</h2>\r\n        <ol class=\"breadcrumb\">\r\n            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 241, "\"", 281, 1);
#line 8 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 248, Url.Action("Dashboard", "Index"), 248, 33, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(282, 66, true);
            WriteLiteral(">Home</a>\r\n            </li>\r\n            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 348, "\"", 394, 1);
#line 11 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 355, Url.Action("Index", "LicenseCategory"), 355, 39, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(395, 99, true);
            WriteLiteral(">License Category</a>\r\n            </li>\r\n            <li class=\"active\">\r\n                <strong>");
            EndContext();
            BeginContext(495, 17, false);
#line 14 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                   Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(512, 155, true);
            WriteLiteral("</strong>\r\n            </li>\r\n        </ol>\r\n    </div>\r\n    <div class=\"col-lg-2 right\">\r\n        <button type=\"button\" class=\"btn btn-w-m btn-primary\"><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 667, "\"", 714, 1);
#line 19 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 674, Url.Action("Create", "LicenseCategory"), 674, 40, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(715, 164, true);
            WriteLiteral(">Create New</a></button>\r\n    </div>\r\n</div>\r\n<div class=\"wrapper wrapper-content animated fadeInRight\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n\r\n");
            EndContext();
#line 26 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
             if (ViewBag.SucMsg != "")
            {

#line default
#line hidden
            BeginContext(934, 42, true);
            WriteLiteral("                <div class=\"view-success\">");
            EndContext();
            BeginContext(977, 24, false);
#line 28 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                     Write(Html.Raw(ViewBag.SucMsg));

#line default
#line hidden
            EndContext();
            BeginContext(1001, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
#line 29 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(1024, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 31 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
             if (ViewBag.ErrMsg != "")
            {

#line default
#line hidden
            BeginContext(1081, 40, true);
            WriteLiteral("                <div class=\"view-error\">");
            EndContext();
            BeginContext(1122, 24, false);
#line 33 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                   Write(Html.Raw(ViewBag.ErrMsg));

#line default
#line hidden
            EndContext();
            BeginContext(1146, 8, true);
            WriteLiteral("</div>\r\n");
            EndContext();
#line 34 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
            }

#line default
#line hidden
            BeginContext(1169, 302, true);
            WriteLiteral(@"
            <div class=""ibox float-e-margins"">
                <div class=""ibox-content"">

                    <table class=""table table-striped table-bordered table-hover dataTables-example"">
                        <thead>
                            <tr>
                                <th>");
            EndContext();
            BeginContext(1472, 32, false);
#line 42 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1504, 43, true);
            WriteLiteral("</th>\r\n                                <th>");
            EndContext();
            BeginContext(1548, 36, false);
#line 43 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.IsActive));

#line default
#line hidden
            EndContext();
            BeginContext(1584, 43, true);
            WriteLiteral("</th>\r\n                                <th>");
            EndContext();
            BeginContext(1628, 38, false);
#line 44 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.LastUpdate));

#line default
#line hidden
            EndContext();
            BeginContext(1666, 6, true);
            WriteLiteral("</th> ");
            EndContext();
            BeginContext(1779, 39, true);
            WriteLiteral(" \r\n                                <th>");
            EndContext();
            BeginContext(1819, 36, false);
#line 45 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                               Write(Html.DisplayNameFor(p => p.LastUser));

#line default
#line hidden
            EndContext();
            BeginContext(1855, 152, true);
            WriteLiteral("</th>\r\n                                <th></th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n");
            EndContext();
#line 50 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                             foreach (LicenseCategory licenseCategory in (List<LicenseCategory>)ViewBag.LicenseCategoryList)
                            {

#line default
#line hidden
            BeginContext(2164, 78, true);
            WriteLiteral("                                <tr>\r\n                                    <td>");
            EndContext();
            BeginContext(2243, 42, false);
#line 53 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                   Write(Html.DisplayFor(p => licenseCategory.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2285, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2333, 46, false);
#line 54 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                   Write(Html.DisplayFor(p => licenseCategory.IsActive));

#line default
#line hidden
            EndContext();
            BeginContext(2379, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2427, 48, false);
#line 55 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                   Write(Html.DisplayFor(p => licenseCategory.LastUpdate));

#line default
#line hidden
            EndContext();
            BeginContext(2475, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2523, 46, false);
#line 56 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                   Write(Html.DisplayFor(p => licenseCategory.LastUser));

#line default
#line hidden
            EndContext();
            BeginContext(2569, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
#line 57 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                     if (ViewBag.IsTrash != "1")
                                    {

#line default
#line hidden
            BeginContext(2681, 46, true);
            WriteLiteral("                                        <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2727, "\"", 2786, 1);
#line 59 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 2734, Url.Action("Edit", new { id = licenseCategory.Id }), 2734, 52, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2787, 14, true);
            WriteLiteral(">Edit</a> | <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2801, "\"", 2862, 1);
#line 59 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 2808, Url.Action("Delete", new { id = licenseCategory.Id }), 2808, 54, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2863, 18, true);
            WriteLiteral(">Delete</a></td>\r\n");
            EndContext();
#line 60 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                    }
                                    else
                                    {

#line default
#line hidden
            BeginContext(3001, 46, true);
            WriteLiteral("                                        <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3047, "\"", 3109, 1);
#line 63 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 3054, Url.Action("Restore", new { id = licenseCategory.Id }), 3054, 55, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3110, 17, true);
            WriteLiteral(">Restore</a> | <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3127, "\"", 3201, 1);
#line 63 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 3134, Url.Action("Delete", new { id = licenseCategory.Id, trash = "1" }), 3134, 67, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3202, 18, true);
            WriteLiteral(">Delete</a></td>\r\n");
            EndContext();
#line 64 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                    }

#line default
#line hidden
            BeginContext(3259, 39, true);
            WriteLiteral("                                </tr>\r\n");
            EndContext();
#line 66 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                            }

#line default
#line hidden
            BeginContext(3329, 66, true);
            WriteLiteral("                        </tbody>\r\n                    </table>\r\n\r\n");
            EndContext();
#line 70 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                     if (ViewBag.IsTrash != "1")
                    {

#line default
#line hidden
            BeginContext(3468, 45, true);
            WriteLiteral("                        <a class=\"trash-link\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3513, "\"", 3561, 1);
#line 72 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 3520, Url.Action("Index", new { trash = "1" }), 3520, 41, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3562, 13, true);
            WriteLiteral(">Trash List (");
            EndContext();
            BeginContext(3576, 18, false);
#line 72 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                                                                                                      Write(ViewBag.TrashCount);

#line default
#line hidden
            EndContext();
            BeginContext(3594, 7, true);
            WriteLiteral(")</a>\r\n");
            EndContext();
#line 73 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                    }
                    else
                    {

#line default
#line hidden
            BeginContext(3673, 50, true);
            WriteLiteral("                        <a class=\"trash-link back\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 3723, "\"", 3750, 1);
#line 76 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
WriteAttributeValue("", 3730, Url.Action("Index"), 3730, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(3751, 19, true);
            WriteLiteral(">Back To List</a>\r\n");
            EndContext();
#line 77 "D:\InCity Source\InCity License Web 2.0\IncityApp.Web\Areas\Backoffice\Views\LicenseCategory\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(3793, 84, true);
            WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Styles", async() => {
                BeginContext(3894, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(3900, 150, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d7d146f4f2eb488584ac47929f4c2a53", async() => {
                    BeginContext(3952, 10, true);
                    WriteLiteral("\r\n        ");
                    EndContext();
                    BeginContext(3962, 68, false);
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "21b8687b14794c278a006dbf4d87c3b2", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    EndContext();
                    BeginContext(4030, 6, true);
                    WriteLiteral("\r\n    ");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4050, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            BeginContext(4055, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(4075, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(4081, 140, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("environment", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "bfa2184657ee45569f455a59c1cf2029", async() => {
                    BeginContext(4133, 10, true);
                    WriteLiteral("\r\n        ");
                    EndContext();
                    BeginContext(4143, 58, false);
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "42f71290ec5c42de9f151a3084e79e25", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                    __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    EndContext();
                    BeginContext(4201, 6, true);
                    WriteLiteral("\r\n    ");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.EnvironmentTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_EnvironmentTagHelper.Names = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4221, 1101, true);
                WriteLiteral(@"

    <script type=""text/javascript"">
        $(document).ready(function () {

            $('.dataTables-example').DataTable({
                pageLength: 25,
                columnDefs: [{ ""type"": ""date"", ""targets"": 2 }],
                order: [[2, ""desc""]],
                dom: '<""html5buttons""B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel' },
                    { extend: 'pdf' },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
   ");
                WriteLiteral("             ]\r\n\r\n            });\r\n\r\n\r\n        });\r\n\r\n      \r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.ApplicationInsights.AspNetCore.JavaScriptSnippet JavaScriptSnippet { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IncityApp.Models.LicenseCategory> Html { get; private set; }
    }
}
#pragma warning restore 1591
