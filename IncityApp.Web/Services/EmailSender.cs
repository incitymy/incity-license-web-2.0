﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace IncityApp.Services
{
    public class EmailSender : IEmailSender
    {
        private string from = "sales@incity.my"; // set sender email.
        private string pwd = "Sales123!!"; // set sender email's password.
        private string host = "yolo.hostlogic.sg";
        //private string host = "mail.incity.my";
        private int port = 25;
        private string subject = "";
        private string body = "";

        public async Task SendEmailAsync(string email, string _subject, string _body)
        {
            var smtpClient = new SmtpClient
            {
                Host = host, // set your SMTP server name here
                Port = port, // Port 
                EnableSsl = false,
                Credentials = new NetworkCredential(from, pwd)
            };

            subject = _subject;
            body = _body;

            using (var _message = new MailMessage(new MailAddress(from, "Advance InCity"), new MailAddress(email))
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
                BodyEncoding = Encoding.UTF8
            })
            {
                await smtpClient.SendMailAsync(_message);
            }
        }
    }
}
