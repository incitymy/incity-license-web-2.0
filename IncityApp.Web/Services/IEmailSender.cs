﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IncityApp.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string _subject, string _body);
    }
}
