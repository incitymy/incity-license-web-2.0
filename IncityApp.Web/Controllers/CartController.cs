using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace IncityApp.Controllers
{
    public class CartController : Controller
    {
        private ILicenseCategoryService licenseCategoryService;
        private ILicenseService licenseService;

        public CartController(ILicenseCategoryService _licenseCategoryService, ILicenseService _licenseServie)
        {
            licenseCategoryService = _licenseCategoryService;
            licenseService = _licenseServie;
        }

        public IActionResult Index()
        {
            Cart cart = HttpContext.Session.GetObject<Cart>("Cart");
            if (cart == null)
            {
                cart = new Cart();
                cart.cartItems = new List<CartItem>();
            }

            return View(cart);
        }

        public IActionResult AddToCart(int id = 0)
        {
            if(id != 0)
            {
                License license = licenseService.GetActive(id, out string ErrMsg);

                if(ErrMsg == "")
                {
                    Cart cart = HttpContext.Session.GetObject<Cart>("Cart");
                    if (cart == null)
                    {
                        cart = new Cart();
                    }

                    CartItem item = new CartItem();
                    item.license = license;
                    item.qty = 1;
                    item.subtotal = license.Price;

                    cart.grandTotal += license.Price;

                    if(cart.cartItems == null)
                    {
                        cart.cartItems = new List<CartItem>();
                    }
                    cart.cartItems.Add(item);
                    HttpContext.Session.SetObject("Cart", cart);
                }
            }
            return RedirectToAction("Index");
        }

        public IActionResult Remove(int id = 0)
        {
            if (id != 0)
            {
                Cart cart = HttpContext.Session.GetObject<Cart>("Cart");
                CartItem toBeRemoved = cart.cartItems.Where(p => p.license.Id == id).FirstOrDefault();

                if (toBeRemoved != null)
                {
                    cart.cartItems.Remove(toBeRemoved);
                    cart.grandTotal -= toBeRemoved.license.Price * toBeRemoved.qty;

                    HttpContext.Session.SetObject("Cart", cart);
                }
            }
            return RedirectToAction("Index");
        }

        public IActionResult UpdateCart(int id = 0, int qty = 1)
        {
            if (id != 0)
            {
                Cart cart = HttpContext.Session.GetObject<Cart>("Cart");
                CartItem item = cart.cartItems.Where(p => p.license.Id == id).FirstOrDefault();

                if (item != null)
                {
                    item.qty = qty;
                    item.subtotal = item.license.Price * item.qty;

                    decimal grandTotal = 0.00m;
                    foreach(CartItem item2 in cart.cartItems)
                    {
                        grandTotal += item2.subtotal;
                    }

                    cart.grandTotal = grandTotal;

                    HttpContext.Session.SetObject("Cart", cart);
                }
            }
            return RedirectToAction("Index");
        }
    }
}