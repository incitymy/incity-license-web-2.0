using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace IncityApp.Controllers
{
    public class MemberController : Controller
    {
        private string pageName = "Member";
        private string encryptKey = "E546C8DF278CD59Member1069B522E69";

        private IMemberService memberService;

        public MemberController(IMemberService _memberService)
        {
            memberService = _memberService;
        }

        // Initiate new object
        private Member NewObject()
        {
            Member member = new Member();
            member.IsActive = true;
            member.IsDeleted = false;
            member.CreateDate = DateTime.Now;
            member.CreateBy = "-";
            member.LastUpdate = DateTime.Now;
            member.LastUser = "-";
            member.Remark = "";

            LoadList(member);

            return member;
        }

        // Load existing object
        private Member LoadObject(int id = 0)
        {
            Member member = new Member();
            if (id != 0)
            {
                member = memberService.GetActive(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;
                }
                else
                {
                    member.Password = memberService.DecryptString(member.Password, encryptKey);
                    member.LastUpdate = DateTime.Now;
                    member.LastUser = User.Identity.Name;
                }
            }

            LoadList(member);

            return member;
        }

        // Pre-load lists
        private void LoadList(Member member)
        {
        }

        public IActionResult Login(string returnUrl = "")
        {
            ViewData["Title"] = "Member List";

            MemberLoginModel member = new MemberLoginModel();

            if(returnUrl == "")
            {
                member.ReturnUrl = Url.Action("Index", "Home");
            }
            else
            {
                member.ReturnUrl = returnUrl;

                if (member.ReturnUrl.ToLower().Contains("checkout"))
                {
                    ViewBag.Message = "Please login to your account or create an account to proceed to checkout.";
                }
            }

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(MemberLoginModel loginDetail)
        {
            ViewData["Title"] = "Member Login";

            Member member = new Member();

            member = memberService.GetMember(loginDetail.Email, loginDetail.Password, encryptKey, out string ErrMsg);

            if (ErrMsg != "")
            {
                ViewBag.ErrMsg = ErrMsg;

                return View(loginDetail);
            }
            else
            {
                if (member != null)
                {
                    // claim 
                    IList<Claim> claims = new List<Claim>
                    {
                        new Claim("MemberId", member.Id.ToString()),
                        new Claim(ClaimTypes.Name, member.Email),
                        new Claim(ClaimTypes.NameIdentifier, member.Id.ToString())
                    };
                    ClaimsIdentity identity = new ClaimsIdentity(claims, "Frontend");
                    var userPrincipal = new ClaimsPrincipal(identity);
                    //User.HasClaim(c => c.Type == "");

                    await HttpContext.SignInAsync("Frontend", userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(60),
                            IsPersistent = true,
                            AllowRefresh = true
                        });

                    return Redirect(loginDetail.ReturnUrl);
                }
                else
                {
                    // A simple checking to prevent someone get to login without an member account.
                    ModelState.AddModelError("", "Login failed, empty account detected.");

                    return View(loginDetail);
                }
            }
        }

        public IActionResult Logout()
        {
            try
            {
                HttpContext.Session.Clear();
                HttpContext.SignOutAsync("Frontend");
            }
            catch (Exception ex)
            {
            }

            return RedirectToAction("Index", "Home");
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            ViewBag.Member = User.Identity.Name;

            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                List<Member> list = memberService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;
                }
                else
                {
                    List<Member> deletedList = memberService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ViewBag.ErrMsg = ErrMsg;
                    }
                    else
                    {
                        ViewBag.MemberList = list;
                        ViewBag.TrashCount = deletedList.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<Member> list = memberService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;
                }
                else
                {
                    ViewBag.MemberList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete.";
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Register(string returnUrl = "")
        {
            ViewData["Title"] = "Create " + pageName;
            Member member = NewObject();

            if (returnUrl == "")
            {
                member.ReturnUrl = Url.Action("Index", "Home");
            }
            else
            {
                member.ReturnUrl = returnUrl;
            }

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(Member member)
        {
            if (ModelState.IsValid)
            {
                member.Password = memberService.EncryptString(member.Password, encryptKey);

                memberService.Create(member, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;

                    LoadList(member);

                    return View(member);
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    // claim 
                    IList<Claim> claims = new List<Claim>
                    {
                        new Claim("MemberId", member.Id.ToString()),
                        new Claim(ClaimTypes.Name, member.Email),
                        new Claim(ClaimTypes.NameIdentifier, member.Id.ToString())
                    };
                    ClaimsIdentity identity = new ClaimsIdentity(claims, "Frontend");
                    var userPrincipal = new ClaimsPrincipal(identity);
                    //User.HasClaim(c => c.Type == "");

                    await HttpContext.SignInAsync("Frontend", userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(60),
                            IsPersistent = true,
                            AllowRefresh = true
                        });

                    return Redirect(member.ReturnUrl);
                }
            }

            return View(member);
        }

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            Member member = LoadObject(id);

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Member member)
        {
            if (ModelState.IsValid)
            {
                member.Password = memberService.EncryptString(member.Password, encryptKey);

                memberService.Edit(member, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    ModelState.Clear();
                }
            }

            member = LoadObject(member.Id);
            return View(member);
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            Member member = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                memberService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                memberService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ViewBag.ErrMsg = ErrMsg;

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            Member member = LoadObject(id);

            return View(member);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            memberService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ViewBag.ErrMsg = ErrMsg;

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}