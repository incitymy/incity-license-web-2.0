using IncityApp.BLL;
using IncityApp.Models;
using Engine;
using IncityApp.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace IncityApp.Controllers
{
    public class CheckoutController : Controller
    {
        private string pageName = "License";
        
        private IMemberService memberService;
        private IOrderService orderService;
        private IOrderDetailService orderDetailService;
        private IEmailSender emailSender;
        private ILicenseCategoryService licenseCategoryService;
        private ILicenseService licenseService;
        
        private CheckoutModel LoadCart(out string ErrMsg)
        {
            CheckoutModel checkoutModel = new CheckoutModel();
            var claim = User.Claims.Where(p => p.Type == "MemberId").FirstOrDefault();
            int id = Int32.Parse(claim.Value.ToString());
            checkoutModel.member = memberService.GetActive(1, out ErrMsg);

            checkoutModel.cart = HttpContext.Session.GetObject<Cart>("Cart");
            if (checkoutModel.cart == null)
            {
                checkoutModel.cart = new Cart();
                checkoutModel.cart.cartItems = new List<CartItem>();
            }

            ErrMsg = "";

            return checkoutModel;
        }

        public CheckoutController(IMemberService _memberService, IOrderService _orderService, IOrderDetailService _orderDetailService, IEmailSender _emailSender,
                                  ILicenseCategoryService _licenseCategoryService, ILicenseService _licenseService)
        {
            memberService = _memberService;
            orderService = _orderService;
            orderDetailService = _orderDetailService;
            emailSender = _emailSender;
            licenseCategoryService = _licenseCategoryService;
            licenseService = _licenseService;
        }

        public IActionResult Index()
        {
            if(!User.HasClaim(p => p.Type == "MemberId"))
            {
                return RedirectToAction("Login", "Member", new { returnUrl = Url.Action("Index", "Checkout") });
            }

            CheckoutModel checkoutModel = LoadCart(out string ErrMsg);
            if (ErrMsg != "")
            {
                return RedirectToAction("Login", "Member", new { returnUrl = Url.Action("Index", "Checkout") });
            }

            return View(checkoutModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(CheckoutModel checkoutModel)
        {
            CheckoutModel _checkoutModel = LoadCart(out string ErrMsg);

            if (ErrMsg != "")
            {
                return RedirectToAction("Login", "Member", new { returnUrl = Url.Action("Index", "Checkout") });
            }

            checkoutModel.cart = _checkoutModel.cart;
            checkoutModel.member = _checkoutModel.member;

            if (ModelState.IsValid)
            {
                orderService.Create(checkoutModel, out ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    return View(checkoutModel);
                }
                else
                {
                    string body = @"<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Advance InCity Sdn Bhd</title>
<style>
    td {
        border: 1px solid #000;
        padding: 5px 10px;
    }
</style>
</head>

<body style='background-color:#eeeeee;'>
<p>&nbsp;</p>
<table width='700' border='0' align='center' cellpadding='40' cellspacing='0' style='width:595px; margin:0 auto; border-collapse: collapse;'>
  <tr>
    <td bgcolor='#FFFFFF' valign=''>
        <center style=''>
            <a href='http://" + Request.Host + @"'><img src='http://" + Request.Host + @"/images/Logo.png' border='0' style='width:200px;margin-top:15px'/></a>
            <div style='font-weight:bold;'>
            <p>Advance InCity Sdn Bhd<br/>" +
            // pro.Address + @"<br/>
            // Tel: " + pro.PhoneNo + @"<br/>
            //Email: " + pro.Email + @"<br/>
            // Website: " + Request.Url.Authority + @"</p>
            @"</div>
    <p style='text-align:center; text-decoration:none;background:#1ab394;padding:10px'><strong>Thank you for your order.</strong></p>
    <p style='text-align:left;'>Order details are shown below for your reference:</p>

    <table style='float:left;text-align:left;width:100%;'>
    <tr>
    <td style='width:45%;font-weight:bold;'>License</td>
    <td style='width:15%;font-weight:bold;'>Qty</td>
    <td style='width:20%;font-weight:bold'>Price (RM)</td>
    <td style='width:20%;font-weight:bold'>SubTotal (RM)</td>
    </tr>";
                    foreach (CartItem item in checkoutModel.cart.cartItems)
                    {
                        License prod = licenseService.GetActive(item.license.Id, out ErrMsg);

                        if (ErrMsg == "")
                        {
                            LicenseCategory cate = licenseCategoryService.GetActive(prod.LicenseCategoryId, out ErrMsg);

                            if (ErrMsg == "")
                            {
                                for (int j = 0; j < item.qty; j++)
                                {
                                    decimal pr = Convert.ToDecimal(item.license.Price) * Convert.ToDecimal(item.qty);
                                    body += @" <tr>
<td>" + cate.Name + item.license.Name + @"</td>
<td>" + item.qty + @"</td>
<td>" + item.license.Price + @"</td>
<td>" + Convert.ToDecimal(item.subtotal).ToString("f2") + @"</td>
</tr>";
                                }
                            }
                        }
                    }
                    body += @" <tr>
    <td style='border-right:0px;font-weight:bold;' colspan='3'>Total</td>
    <td>" + checkoutModel.cart.grandTotal.ToString("f2") + @"</td>
    </tr>
     <tr>
    <td style='border-right:0px;font-weight:bold' colspan='3'>Payment Method</td>
    <td>" + checkoutModel.PaymentMethod + @"</td>
    </tr>
    </table>
    <br />
            <div style='width:100%;display:inline-block'>
            <p style='text-align:left;font-weight:bold;font-size:18px'>Order Date:" + DateTime.Today.ToString("dd/MM/yyyy") + @"</p>
            </div>
       
       <p style='margin-top:100px;padding-bottom:30px'>� 2018 Advance InCity Sdn Bhd. All Right Reserved.</p>
        </center>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
";
                    //emailSender.SendEmailAsync(checkoutModel.member.Email, "Order Details", body).Wait();
                    //emailSender.SendEmailAsync("sales@incity.my", "Order Details", body).Wait();

                    ModelState.Clear();
                    HttpContext.Session.Clear();
                    return RedirectToAction("Success");
                }
            }

            return View(checkoutModel);
        }

        public IActionResult Success()
        {
            return View();
        }
    }
}