using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace IncityApp.Controllers
{
    public class HomeController : Controller
    {
        private ILicenseCategoryService licenseCategoryService;
        private ILicenseService licenseService;

        public HomeController(ILicenseCategoryService _licenseCategoryService, ILicenseService _licenseServie)
        {
            licenseCategoryService = _licenseCategoryService;
            licenseService = _licenseServie;
        }

        public IActionResult Index()
        {
            HomeViewModel home = new HomeViewModel();
            home.licenseCategories = licenseCategoryService.GetActiveList(out string ErrMsg);
            home.licenses = licenseService.GetActiveList(out ErrMsg);

            string member = HttpContext.Session.GetString("Member");
            if(member != null || member != "")
            {
                ViewBag.Member = member;
            }

            return View(home);
        }
    }
}