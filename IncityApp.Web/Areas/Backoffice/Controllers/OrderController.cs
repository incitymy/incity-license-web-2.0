using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Controllers
{
    [Area("Backoffice")]
    [Authorize(Policy = "Admin")]
    public class OrderController : Controller
    {
        private string pageName = "Order";

        private IOrderService orderService;
        private IOrderDetailService orderDetailService;
        private IAdminLogService adminLogService;

        public OrderController(IOrderService _orderService, IOrderDetailService _orderDetailService, IAdminLogService _adminLogServie)
        {
            orderService = _orderService;
            orderDetailService = _orderDetailService;
            adminLogService = _adminLogServie;
        }

        // Initiate new object
        private Order NewObject()
        {
            Order order = new Order();
            order.IsActive = true;
            order.IsDeleted = false;
            order.CreateDate = DateTime.Now;
            order.CreateBy = User.Identity.Name;
            order.LastUpdate = DateTime.Now;
            order.LastUser = User.Identity.Name;
            order.Remark = "";

            return order;
        }

        // Load existing object
        private Order LoadObject(int id = 0)
        {
            Order order = new Order();
            if (id != 0)
            {
                order = orderService.GetActive(id, out string ErrMsg);
                order.StatusList = new SelectList(
                    new List<SelectListItem>
                    {
                        new SelectListItem { Text = "Pending", Value = "Pending"},
                        new SelectListItem { Text = "Success", Value = "Success"},
                        new SelectListItem { Text = "Failed", Value = "Failed"},
                        new SelectListItem { Text = "Cancelled", Value = "Cancelled"}
                    }, "Status", "Status");
                order.orderDetails = orderDetailService.GetActiveList(out ErrMsg);
                order.LastUpdate = DateTime.Now;
                order.LastUser = User.Identity.Name;

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
            }

            return order;
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                ViewBag.OrderList = orderService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    List<Order> list = orderService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ModelState.AddModelError(pageName, ErrMsg);
                    }
                    else
                    {
                        ViewBag.TrashCount = list.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<Order> list = orderService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    ViewBag.OrderList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete."; 
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Create()
        {
            ViewData["Title"] = "Create " + pageName;
            Order order = NewObject();

            return View(order);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Create(Order order)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        orderService.Create(order, out string ErrMsg);

        //        if (ErrMsg != "")
        //        {
        //            ModelState.AddModelError(pageName, ErrMsg);

        //            adminLogService.Create("Create (Id: " + order.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

        //            if(ErrMsg2 != "")
        //            {
        //                ViewBag.ErrMsg = "Warning: Failed to create admin log.";
        //            }

        //            return View();
        //        }
        //        else
        //        {
        //            ViewBag.SucMsg = "Succesfully";

        //            adminLogService.Create("Create (Id: " + order.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

        //            if (ErrMsg2 != "")
        //            {
        //                ViewBag.ErrMsg = "Warning: Failed to create admin log.";
        //            }

        //            order = NewObject();
        //            ModelState.Clear();
        //            return View(order);
        //        }
        //    }

        //    return View();
        //}

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            Order order = LoadObject(id);

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                orderService.Edit(order, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Edit (Id: " + order.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Edit (Id: " + order.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
            }

            return View();
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            Order order = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                orderService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Successfully Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                orderService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            Order order = LoadObject(id);

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            orderService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ModelState.AddModelError(pageName, ErrMsg);

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}