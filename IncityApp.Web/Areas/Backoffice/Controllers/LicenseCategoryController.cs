using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace IncityApp.Controllers
{
    [Area("Backoffice")]
    [Authorize(Policy = "Admin")]
    public class LicenseCategoryController : Controller
    {
        private string pageName = "License Category";

        private ILicenseCategoryService licenseCategoryService;
        private IAdminLogService adminLogService;

        public LicenseCategoryController(ILicenseCategoryService _licenseCategoryService, IAdminLogService _adminLogServie)
        {
            licenseCategoryService = _licenseCategoryService;
            adminLogService = _adminLogServie;
        }

        // Initiate new object
        private LicenseCategory NewObject()
        {
            LicenseCategory licenseCategory = new LicenseCategory();
            licenseCategory.IsActive = true;
            licenseCategory.IsDeleted = false;
            licenseCategory.CreateDate = DateTime.Now;
            licenseCategory.CreateBy = User.Identity.Name;
            licenseCategory.LastUpdate = DateTime.Now;
            licenseCategory.LastUser = User.Identity.Name;
            licenseCategory.Remark = "";

            return licenseCategory;
        }

        // Load existing object
        private LicenseCategory LoadObject(int id = 0)
        {
            LicenseCategory licenseCategory = new LicenseCategory();
            if (id != 0)
            {
                licenseCategory = licenseCategoryService.GetActive(id, out string ErrMsg);
                licenseCategory.LastUpdate = DateTime.Now;
                licenseCategory.LastUser = User.Identity.Name;

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
            }

            return licenseCategory;
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                ViewBag.LicenseCategoryList = licenseCategoryService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    List<LicenseCategory> list = licenseCategoryService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ModelState.AddModelError(pageName, ErrMsg);
                    }
                    else
                    {
                        ViewBag.TrashCount = list.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<LicenseCategory> list = licenseCategoryService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    ViewBag.LicenseCategoryList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete."; 
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Create()
        {
            ViewData["Title"] = "Create " + pageName;
            LicenseCategory licenseCategory = NewObject();

            return View(licenseCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(LicenseCategory licenseCategory)
        {
            if (ModelState.IsValid)
            {
                licenseCategoryService.Create(licenseCategory, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Create (Id: " + licenseCategory.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if(ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Create (Id: " + licenseCategory.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    licenseCategory = NewObject();
                    ModelState.Clear();
                    return View(licenseCategory);
                }
            }

            return View();
        }

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            LicenseCategory licenseCategory = LoadObject(id);

            return View(licenseCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(LicenseCategory licenseCategory)
        {
            if (ModelState.IsValid)
            {
                licenseCategoryService.Edit(licenseCategory, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Edit (Id: " + licenseCategory.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Edit (Id: " + licenseCategory.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
            }

            return View();
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            LicenseCategory licenseCategory = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(licenseCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                licenseCategoryService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Successfully Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                licenseCategoryService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            LicenseCategory licenseCategory = LoadObject(id);

            return View(licenseCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            licenseCategoryService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ModelState.AddModelError(pageName, ErrMsg);

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}