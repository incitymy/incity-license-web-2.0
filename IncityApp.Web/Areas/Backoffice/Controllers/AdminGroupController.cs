using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Controllers
{
    [Area("Backoffice")]
    [Authorize(Policy = "Admin")]
    public class AdminGroupController : Controller
    {
        private string pageName = "Admin Group";

        private IAdminGroupService adminGroupService;
        private IAdminLogService adminLogService;

        public AdminGroupController(IAdminGroupService _adminGroupService, IAdminLogService _adminLogServie)
        {
            adminGroupService = _adminGroupService;
            adminLogService = _adminLogServie;
        }

        // Initiate new object
        private AdminGroup NewObject()
        {
            AdminGroup adminGroup = new AdminGroup();
            adminGroup.IsActive = true;
            adminGroup.IsDeleted = false;
            adminGroup.CreateDate = DateTime.Now;
            adminGroup.CreateBy = User.Identity.Name;
            adminGroup.LastUpdate = DateTime.Now;
            adminGroup.LastUser = User.Identity.Name;
            adminGroup.Remark = "";

            return adminGroup;
        }

        // Load existing object
        private AdminGroup LoadObject(int id = 0)
        {
            AdminGroup adminGroup = new AdminGroup();
            if (id != 0)
            {
                adminGroup = adminGroupService.GetActive(id, out string ErrMsg);
                adminGroup.LastUpdate = DateTime.Now;
                adminGroup.LastUser = User.Identity.Name;

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
            }

            return adminGroup;
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                List<AdminGroup> list = adminGroupService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    if(!User.HasClaim(p => p.Type == "Super Admin"))
                    {
                        AdminGroup super = list.Where(p => p.Name == "Super Admin").FirstOrDefault();
                        list.Remove(super);
                    }

                    List<AdminGroup> deletedList = adminGroupService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ModelState.AddModelError(pageName, ErrMsg);
                    }
                    else
                    {
                        if (!User.HasClaim(p => p.Type == "Super Admin"))
                        {
                            AdminGroup super = list.Where(p => p.Name == "Super Admin").FirstOrDefault();
                            list.Remove(super);
                        }
                        ViewBag.AdminGroupList = list;
                        ViewBag.TrashCount = deletedList.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<AdminGroup> list = adminGroupService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    if (!User.HasClaim(p => p.Type == "Super Admin"))
                    {
                        AdminGroup super = list.Where(p => p.Name == "Super Admin").FirstOrDefault();
                        list.Remove(super);
                    }

                    ViewBag.AdminGroupList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete."; 
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Create()
        {
            ViewData["Title"] = "Create " + pageName;
            AdminGroup adminGroup = NewObject();

            return View(adminGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AdminGroup adminGroup)
        {
            if (ModelState.IsValid)
            {
                adminGroupService.Create(adminGroup, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Create (Id: " + adminGroup.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if(ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View(adminGroup);
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Create (Id: " + adminGroup.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    adminGroup = NewObject();
                    ModelState.Clear();
                    return View(adminGroup);
                }
            }

            return View(adminGroup);
        }

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            AdminGroup adminGroup = LoadObject(id);

            return View(adminGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(AdminGroup adminGroup)
        {
            if (ModelState.IsValid)
            {
                adminGroupService.Edit(adminGroup, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Edit (Id: " + adminGroup.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Edit (Id: " + adminGroup.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    ModelState.Clear();
                }
            }

            adminGroup = LoadObject(adminGroup.Id);
            return View(adminGroup);
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            AdminGroup adminGroup = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(adminGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                adminGroupService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Successfully Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                adminGroupService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            AdminGroup adminGroup = LoadObject(id);

            return View(adminGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            adminGroupService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ModelState.AddModelError(pageName, ErrMsg);

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}