using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Controllers
{
    [Area("Backoffice")]
    [Authorize(Policy = "IsAdmin")]
    public class LicenseController : Controller
    {
        private string pageName = "License";

        private ILicenseService licenseService;
        private ILicenseCategoryService licenseCategoryService;
        private IAdminLogService adminLogService;

        public LicenseController(ILicenseService _licenseService, ILicenseCategoryService _licenseCategoryService, IAdminLogService _adminLogServie)
        {
            licenseService = _licenseService;
            licenseCategoryService = _licenseCategoryService;
            adminLogService = _adminLogServie;
        }

        // Initiate new object
        private License NewObject()
        {
            License license = new License();
            license.IsActive = true;
            license.IsDeleted = false;
            license.CreateDate = DateTime.Now;
            license.CreateBy = User.Identity.Name;
            license.LastUpdate = DateTime.Now;
            license.LastUser = User.Identity.Name;
            license.Remark = "";

            LoadList(license);

            return license;
        }

        // Load existing object
        private License LoadObject(int id = 0)
        {
            License license = new License();
            if (id != 0)
            {
                license = licenseService.GetActive(id, out string ErrMsg);
                license.LastUpdate = DateTime.Now;
                license.LastUser = User.Identity.Name;

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
            }

            LoadList(license);

            return license;
        }

        // Pre-load lists
        private void LoadList(License license)
        {
            license.LicenseCategories = new SelectList(licenseCategoryService.GetActiveList(out string ErrMsg), "Id", "Name");
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                List<License> list = licenseService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    foreach(License license in list)
                    {
                        license.LicenseCategory = licenseCategoryService.GetActive(license.LicenseCategoryId, out ErrMsg);

                        if (ErrMsg != "")
                        {
                            ModelState.AddModelError(pageName, ErrMsg);
                        }
                    }

                    List<License> deletedList = licenseService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ModelState.AddModelError(pageName, ErrMsg);
                    }
                    else
                    {
                        ViewBag.LicenseList = list;
                        ViewBag.TrashCount = deletedList.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<License> list = licenseService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    foreach (License license in list)
                    {
                        license.LicenseCategory = licenseCategoryService.GetActive(license.LicenseCategoryId, out ErrMsg);

                        if (ErrMsg != "")
                        {
                            ModelState.AddModelError(pageName, ErrMsg);
                        }
                    }

                    ViewBag.LicenseList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete."; 
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Create()
        {
            ViewData["Title"] = "Create " + pageName;
            License license = NewObject();

            return View(license);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(License license)
        {
            if (ModelState.IsValid)
            {
                licenseService.Create(license, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Create (Id: " + license.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if(ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    LoadList(license);

                    return View(license);
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Create (Id: " + license.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    license = NewObject();
                    ModelState.Clear();
                    return View(license);
                }
            }

            return View(license);
        }

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            License license = LoadObject(id);

            return View(license);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(License license)
        {
            if (ModelState.IsValid)
            {
                licenseService.Edit(license, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Edit (Id: " + license.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Edit (Id: " + license.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    ModelState.Clear();
                }
            }

            license = LoadObject(license.Id);
            return View(license);
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            License license = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(license);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                licenseService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Successfully Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                licenseService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            License license = LoadObject(id);

            return View(license);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            licenseService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ModelState.AddModelError(pageName, ErrMsg);

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}