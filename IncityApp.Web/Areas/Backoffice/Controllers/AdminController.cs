using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace IncityApp.Controllers
{
    [Area("Backoffice")]
    [Authorize(Policy = "IsAdmin")]
    public class AdminController : Controller
    {
        private string pageName = "Admin";
        private string encryptKey = "E546C8DF278CD59Admin31069B522E69";

        private IAdminService adminService;
        private IAdminGroupService adminGroupService;
        private IAdminLogService adminLogService;

        public AdminController(IAdminService _adminService, IAdminGroupService _adminGroupService, IAdminLogService _adminLogServie)
        {
            adminService = _adminService;
            adminGroupService = _adminGroupService;
            adminLogService = _adminLogServie;
        }

        // Initiate new object
        private Admin NewObject()
        {
            Admin admin = new Admin();
            admin.IsActive = true;
            admin.IsDeleted = false;
            admin.CreateDate = DateTime.Now;
            admin.CreateBy = User.Identity.Name;
            admin.LastUpdate = DateTime.Now;
            admin.LastUser = User.Identity.Name;
            admin.Remark = "";

            LoadList(admin);

            return admin;
        }

        // Load existing object
        private Admin LoadObject(int id = 0)
        {
            Admin admin = new Admin();
            if (id != 0)
            {
                admin = adminService.GetActive(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    admin.Password = adminService.DecryptString(admin.Password, encryptKey);
                    admin.LastUpdate = DateTime.Now;
                    admin.LastUser = User.Identity.Name;
                }
            }

            LoadList(admin);

            return admin;
        }

        // Pre-load lists
        private void LoadList(Admin admin)
        {
            admin.AdminGroups = new SelectList(adminGroupService.GetActiveList(out string ErrMsg), "Id", "Name");
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            ViewData["Title"] = "Admin List";

            AdminLoginModel admin = new AdminLoginModel();

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(AdminLoginModel loginDetail)
        {
            ViewData["Title"] = "Admin Login";

            Admin admin = new Admin();
            admin = adminService.GetAdmin(loginDetail.Username, loginDetail.Password, encryptKey, out string ErrMsg);

            if (ErrMsg != "")
            {
                ViewBag.ErrMsg = "Login failed. Incorrect username or password.";

                adminLogService.Create("Login Failed", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg += Environment.NewLine + "Warning: Failed to create admin log.";
                }
                return View();
            }
            else
            {
                if (admin != null)
                {
                    // claim 
                    string isSuper = "false";
                    if(admin.AdminGroup.Name == "Super Admin")
                    {
                        isSuper = "true";
                    }
                    IList<Claim> claims = new List<Claim>
                    {
                        new Claim("AdminId", admin.Id.ToString()),
                        new Claim("AdminGroup", admin.AdminGroup.Name),
                        new Claim("Super Admin", isSuper),
                        new Claim(ClaimTypes.Name, admin.Username),
                        new Claim(ClaimTypes.NameIdentifier, admin.Id.ToString())
                    };
                    ClaimsIdentity identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    var userPrincipal = new ClaimsPrincipal(identity);
                    //User.HasClaim(c => c.Type == "");

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, userPrincipal,
                        new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(60),
                            IsPersistent = true,
                            AllowRefresh = true
                        });

                    adminLogService.Create("Successfully Login", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg += Environment.NewLine + "Warning: Failed to create admin log.";
                    }
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    // A simple checking to prevent someone get to login without an admin account.
                    ModelState.AddModelError("", "Login failed. Incorrect username or password.");

                    adminLogService.Create("Login Failed", pageName, HttpUtil.GetIPAddress(HttpContext), "Login success but admin is null.", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg += Environment.NewLine + "Warning: Failed to create admin log.";
                    }
                    return View();
                }
            }
        }

        public IActionResult Logout()
        {
            try
            {
                HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                adminLogService.Create("Logged Out", pageName, HttpUtil.GetIPAddress(HttpContext), "", out string ErrMsg);
            }
            catch (Exception ex)
            {
                adminLogService.Create("Logout Failed", pageName, HttpUtil.GetIPAddress(HttpContext), ex.ToString(), out string ErrMsg);
            }

            return RedirectToAction("Login");
        }

        // Index page
        public IActionResult Index(string trash, string delete, string restore)
        {
            ViewBag.Admin = User.Identity.Name;

            if (trash != "1")
            {
                ViewData["Title"] = pageName + " Listing";

                List<Admin> list = adminService.GetActiveList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    foreach (Admin admin in list)
                    {
                        admin.AdminGroup = adminGroupService.GetActive(admin.AdminGroupId, out ErrMsg);

                        if (ErrMsg != "")
                        {
                            ModelState.AddModelError(pageName, ErrMsg);
                        }
                    }

                    List<Admin> deletedList = adminService.GetDeletedList(out ErrMsg);

                    if (ErrMsg != "")
                    {
                        ModelState.AddModelError(pageName, ErrMsg);
                    }
                    else
                    {
                        ViewBag.AdminList = list;
                        ViewBag.TrashCount = deletedList.Count();
                    }
                }
            }
            else
            {
                ViewData["Title"] = "Trash List: " + pageName;

                List<Admin> list = adminService.GetDeletedList(out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);
                }
                else
                {
                    foreach (Admin admin in list)
                    {
                        admin.AdminGroup = adminGroupService.GetActive(admin.AdminGroupId, out ErrMsg);

                        if (ErrMsg != "")
                        {
                            ModelState.AddModelError(pageName, ErrMsg);
                        }
                    }

                    ViewBag.AdminList = list;
                    ViewBag.IsTrash = trash;
                }
            }

            if (delete == "1")
            {
                ViewBag.SucMsg = "Successfully delete.";
            }
            else if (delete == "2")
            {
                ViewBag.SucMsg = "Successfully delete permanant.";
            }

            if (restore == "1")
            {
                ViewBag.SucMsg = "Successfully restore.";
            }

            return View();
        }

        // Create
        public IActionResult Create()
        {
            ViewData["Title"] = "Create " + pageName;
            Admin admin = NewObject();

            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Admin admin)
        {
            if (ModelState.IsValid)
            {
                admin.Password = adminService.EncryptString(admin.Password, encryptKey);

                adminService.Create(admin, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Create (Id: " + admin.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    LoadList(admin);

                    return View(admin);
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Create (Id: " + admin.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    admin = NewObject();
                    ModelState.Clear();
                    return View(admin);
                }
            }

            return View(admin);
        }

        // Edit
        public IActionResult Edit(int id = 0)
        {
            ViewData["Title"] = "Edit " + pageName;
            Admin admin = LoadObject(id);

            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Admin admin)
        {
            if (ModelState.IsValid)
            {
                admin.Password = adminService.EncryptString(admin.Password, encryptKey);

                adminService.Edit(admin, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Edit (Id: " + admin.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Edit (Id: " + admin.Id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    ModelState.Clear();
                }
            }

            admin = LoadObject(admin.Id);
            return View(admin);
        }

        // Delete
        public IActionResult Delete(int id, string trash)
        {
            Admin admin = LoadObject(id);

            if (trash == "1")
            {
                ViewBag.Permanant = "1";
            }
            else
            {
                ViewBag.Permanant = "0";
            }

            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, int permanant)
        {
            if (permanant == 0)
            {
                adminService.Delete(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Successfully Delete (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { delete = "1" });
                }
            }
            else
            {
                adminService.DeletePermanant(id, out string ErrMsg);

                if (ErrMsg != "")
                {
                    ModelState.AddModelError(pageName, ErrMsg);

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return View();
                }
                else
                {
                    ViewBag.SucMsg = "Succesfully";

                    adminLogService.Create("Delete Permanant (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                    if (ErrMsg2 != "")
                    {
                        ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                    }

                    return RedirectToAction("Index", new { trash = "1", delete = "2" });
                }
            }
        }

        // Restore
        public IActionResult Restore(int id)
        {
            Admin admin = LoadObject(id);

            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Restore(int id, string trash)
        {
            adminService.Restore(id, out string ErrMsg);

            if (ErrMsg != "")
            {
                ModelState.AddModelError(pageName, ErrMsg);

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), ErrMsg, out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return View();
            }
            else
            {
                ViewBag.SucMsg = "Succesfully";

                adminLogService.Create("Restore (Id: " + id + ")", pageName, HttpUtil.GetIPAddress(HttpContext), "Successfully", out string ErrMsg2);

                if (ErrMsg2 != "")
                {
                    ViewBag.ErrMsg = "Warning: Failed to create admin log.";
                }

                return RedirectToAction("Index", new { trash = "1", restore = "1" });
            }
        }
    }
}