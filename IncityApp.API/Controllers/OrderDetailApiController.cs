﻿using IncityApp.BLL;
using IncityApp.Models;
using Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace IncityApp.API.Controllers
{
    [Route("api/orderdetailapi")]
    [ApiController]
    public class OrderDetailApiController : ControllerBase
    {
        private string pageName = "OrderDetail";

        private IMemberService memberService;
        private IOrderService orderService;
        private IOrderDetailService orderDetailService;

        public OrderDetailApiController(IMemberService _memberService, IOrderService _orderService, IOrderDetailService _orderDetailService)
        {
            memberService = _memberService;
            orderService = _orderService;
            orderDetailService = _orderDetailService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST api/values
        [HttpPost("checklicense")]
        public ActionResult<ResponseModel> CheckOrderDetailCodeGet(CheckLicenseModel checkLicenseModel)
        {
            orderDetailService.CheckLicenseCode(checkLicenseModel, out ResponseModel response);

            return response;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
