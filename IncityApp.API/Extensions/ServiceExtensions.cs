﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using IncityApp.Models;
using IncityApp.DAL;
using IncityApp.DAL.Repository;
using IncityApp.BLL;
using Microsoft.AspNetCore.Identity;

namespace IncityApp.Extensions
{
    public static class ServiceExtensions
    {
        // CORS Configuration
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        // IIS Configuration
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }

        // MySQL Configuration
        public static void ConfigureMySqlContext(this IServiceCollection services, IConfiguration config)
        {
            // Get connection string from appsettings.json
            //var connectionString = "server=localhost;userid=root;database=incity_license;";
            var connectionString = "server=52.220.205.228;userid=dbuser@incity.my;password=Incity12345!;database=license;";
            //config["mysqlconnection:connectionString"];            
            services.AddDbContext<RepositoryContext>(o => o.UseMySql(connectionString));
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            // Inject repository base
            services.AddScoped<IRepositoryBase<Admin>, RepositoryBase<Admin>>();
            services.AddScoped<IRepositoryBase<AdminGroup>, RepositoryBase<AdminGroup>>();
            services.AddScoped<IRepositoryBase<AdminLog>, RepositoryBase<AdminLog>>();
            services.AddScoped<IRepositoryBase<License>, RepositoryBase<License>>();
            services.AddScoped<IRepositoryBase<LicenseCategory>, RepositoryBase<LicenseCategory>>();
            services.AddScoped<IRepositoryBase<Member>, RepositoryBase<Member>>();
            services.AddScoped<IRepositoryBase<Order>, RepositoryBase<Order>>();
            services.AddScoped<IRepositoryBase<OrderDetail>, RepositoryBase<OrderDetail>>();

            // Inject repository
            services.AddScoped<IAdminRepository, AdminRepository>();
            services.AddScoped<IAdminGroupRepository, AdminGroupRepository>();
            services.AddScoped<IAdminLogRepository, AdminLogRepository>();
            services.AddScoped<ILicenseRepository, LicenseRepository>();
            services.AddScoped<ILicenseCategoryRepository, LicenseCategoryRepository>();
            services.AddScoped<IMemberRepository, MemberRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();

            // Inject service
            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<IAdminGroupService, AdminGroupService>();
            services.AddTransient<IAdminLogService, AdminLogService>();
            services.AddTransient<ILicenseService, LicenseService>();
            services.AddTransient<ILicenseCategoryService, LicenseCategoryService>();
            services.AddTransient<IMemberService, MemberService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderDetailService, OrderDetailService>();
        }
    }
}
