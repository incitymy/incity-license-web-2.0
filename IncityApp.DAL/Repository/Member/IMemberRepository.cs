﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IMemberRepository : IRepositoryBase<Member>
    {
        Member Get(int id, out string ErrMsg);
        Member GetActive(int id, out string ErrMsg);
        List<Member> GetActiveList(out string ErrMsg);
        List<Member> GetDeletedList(out string ErrMsg);
        void Create(Member member, out string ErrMsg);
        void Edit(Member member, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, bool isEdit, out string ErrMsg);
        bool VerifyLogin(string username, string password, out string ErrMsg);
    }
}
