﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class MemberRepository : RepositoryBase<Member>, IMemberRepository
    {
        private string selfName = "IncityApp.DAL.Repository.MemberRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "member";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<Member> memberRepo;

        public MemberRepository(RepositoryContext repositoryContext, IRepositoryBase<Member> _memberRepo)
            : base(repositoryContext)
        {
            memberRepo = _memberRepo;
        }

        public Member Get(int id, out string ErrMsg)
        {
            Member result = new Member();
            try
            {
                result = memberRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public Member GetActive(int id, out string ErrMsg)
        {
            Member result = new Member();
            try
            {
                result = memberRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Member> GetActiveList(out string ErrMsg)
        {
            List<Member> result = new List<Member>();
            try
            {
                result = memberRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Member> GetDeletedList(out string ErrMsg)
        {
            List<Member> result = new List<Member>();
            try
            {
                result = memberRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(Member member, out string ErrMsg)
        {
            try
            {
                memberRepo.Create(member);
                memberRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(Member member, out string ErrMsg)
        {
            try
            {
                memberRepo.Update(member);
                memberRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                Member member = memberRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                member.IsDeleted = true;
                memberRepo.Update(member);
                memberRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                Member member = memberRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                memberRepo.Delete(member);
                memberRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                Member member = memberRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                member.IsDeleted = false;
                memberRepo.Update(member);
                memberRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<Member> member = memberRepo.GetByCondition(p => p.Email == name).ToList();

                if (isEdit == false && member.Count > 0)
                {
                    ErrMsg = "Email already registered.";
                }
                else if (isEdit == true && member.Count > 1)
                {
                    ErrMsg = "Email already registered.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public bool VerifyLogin(string username, string password, out string ErrMsg)
        {
            bool result = false;
            Member member = new Member();
            try
            {
                member = memberRepo.GetByCondition(p => p.Email == username && p.Password == password).FirstOrDefault();
                if (member != null)
                {
                    if (member.IsActive == true && member.IsDeleted == false)
                    {
                        ErrMsg = "";
                        result = true;
                    }
                    else if (member.IsDeleted == true)
                    {
                        ErrMsg = "Login failed. This account has been suspended.";
                    }
                    else if (member.IsActive == false)
                    {
                        ErrMsg = "Login failed. This account has not activate.";
                    }
                    else
                    {
                        ErrMsg = "Login error. Please try again. If error persists, contact the software vendor.";
                    }
                }
                else
                {
                    ErrMsg = "Login failed. Incorrect username or password.";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }
    }
}
