﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IAdminLogRepository : IRepositoryBase<AdminLog>
    {
        List<AdminLog> GetActiveList(out string ErrMsg);
        void Create(AdminLog userLog, out string ErrMsg);
    }
}
