﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class AdminLogRepository : RepositoryBase<AdminLog>, IAdminLogRepository
    {
        private string selfName = "IncityApp.DAL.Repository.AdminLogRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.result
        private string className = "admin log";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<AdminLog> adminLogRepo;

        public AdminLogRepository(RepositoryContext repositoryContext, IRepositoryBase<AdminLog> _adminLogRepo)
            : base(repositoryContext)
        {
            adminLogRepo = _adminLogRepo;
        }

        public List<AdminLog> GetActiveList(out string ErrMsg)
        {
            List<AdminLog> result = new List<AdminLog>();
            try
            {
                result = adminLogRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();

                if(result != null)
                {
                    ErrMsg = "";
                }
                else
                {
                    ErrMsg = "There is no active " + className + ".";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(AdminLog adminLog, out string ErrMsg)
        {
            try
            {
                adminLogRepo.Create(adminLog);
                adminLogRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }
    }
}
