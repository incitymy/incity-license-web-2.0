﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class LicenseRepository : RepositoryBase<License>, ILicenseRepository
    {
        private string selfName = "IncityApp.DAL.Repository.LicenseRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "license ";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<LicenseCategory> licenseCategoryRepo;
        private IRepositoryBase<License> licenseRepo;

        public LicenseRepository(RepositoryContext repositoryContext, IRepositoryBase<LicenseCategory> _licenseCategoryRepo, IRepositoryBase<License> _licenseRepo)
            : base(repositoryContext)
        {
            licenseCategoryRepo = _licenseCategoryRepo;
            licenseRepo = _licenseRepo;
        }

        public License Get(int id, out string ErrMsg)
        {
            License result = new License();
            try
            {
                result = licenseRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public License GetActive(int id, out string ErrMsg)
        {
            License result = new License();
            try
            {
                result = licenseRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<License> GetActiveList(out string ErrMsg)
        {
            List<License> result = new List<License>();
            try
            {
                result = licenseRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<License> GetDeletedList(out string ErrMsg)
        {
            List<License> result = new List<License>();
            try
            {
                result = licenseRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(License license, out string ErrMsg)
        {
            try
            {
                licenseRepo.Create(license);
                licenseRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(License license, out string ErrMsg)
        {
            try
            {
                licenseRepo.Update(license);
                licenseRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                License license = licenseRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                license.IsDeleted = true;
                licenseRepo.Update(license);
                licenseRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                License license = licenseRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                licenseRepo.Delete(license);
                licenseRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                License license = licenseRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                license.IsDeleted = false;
                licenseRepo.Update(license);
                licenseRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, int categoryId, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<License> license = licenseRepo.GetByCondition(p => p.Name == name && p.LicenseCategoryId == categoryId).ToList();

                if (isEdit == false && license.Count > 0)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else if (isEdit == true && license.Count > 1)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }
    }
}
