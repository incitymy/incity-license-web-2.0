﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface ILicenseRepository : IRepositoryBase<License>
    {
        License Get(int id, out string ErrMsg);
        License GetActive(int id, out string ErrMsg);
        List<License> GetActiveList(out string ErrMsg);
        List<License> GetDeletedList(out string ErrMsg);
        void Create(License license, out string ErrMsg);
        void Edit(License license, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, int categoryId, bool isEdit, out string ErrMsg);
    }
}
