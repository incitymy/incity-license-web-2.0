﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;
using Microsoft.EntityFrameworkCore;

namespace IncityApp.DAL.Repository
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        private string selfName = "IncityApp.DAL.Repository.OrderRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "order";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<License> licenseRepo;
        private IRepositoryBase<Order> orderRepo;

        public OrderRepository(RepositoryContext repositoryContext, IRepositoryBase<License> _licenseRepo, IRepositoryBase<Order> _orderRepo)
            : base(repositoryContext)
        {
            licenseRepo = _licenseRepo;
            orderRepo = _orderRepo;
        }

        public Order Get(int id, out string ErrMsg)
        {
            Order result = new Order();
            try
            {
                result = orderRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public Order GetActive(int id, out string ErrMsg)
        {
            Order result = new Order();
            try
            {
                result = orderRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Order> GetActiveList(out string ErrMsg)
        {
            List<Order> result = new List<Order>();
            try
            {
                result = orderRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Order> GetDeletedList(out string ErrMsg)
        {
            List<Order> result = new List<Order>();
            try
            {
                result = orderRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(Order order, out string ErrMsg)
        {
            try
            {
                orderRepo.Create(order);
                orderRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(Order order, out string ErrMsg)
        {
            try
            {
                RepositoryContext.Entry(order).State = EntityState.Modified;
                orderRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                Order order = orderRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                order.IsDeleted = true;
                orderRepo.Update(order);
                orderRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                Order order = orderRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                orderRepo.Delete(order);
                orderRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                Order order = orderRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                order.IsDeleted = false;
                orderRepo.Update(order);
                orderRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<Order> order = orderRepo.GetByCondition(p => p.RefNo == name).ToList();

                if (isEdit == false && order.Count > 0)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else if(isEdit == true && order.Count > 1)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }
    }
}
