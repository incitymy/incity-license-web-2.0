﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        Order Get(int id, out string ErrMsg);
        Order GetActive(int id, out string ErrMsg);
        List<Order> GetActiveList(out string ErrMsg);
        List<Order> GetDeletedList(out string ErrMsg);
        void Create(Order order, out string ErrMsg);
        void Edit(Order order, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, bool isEdit, out string ErrMsg);
    }
}
