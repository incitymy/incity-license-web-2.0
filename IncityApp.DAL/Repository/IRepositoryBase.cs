﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IncityApp.DAL
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
