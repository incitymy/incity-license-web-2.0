﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;
using Microsoft.EntityFrameworkCore;

namespace IncityApp.DAL.Repository
{
    public class LicenseCategoryRepository : RepositoryBase<LicenseCategory>, ILicenseCategoryRepository
    {
        private string selfName = "IncityApp.DAL.Repository.LicenseCategoryRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "license category";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<License> licenseRepo;
        private IRepositoryBase<LicenseCategory> licenseCategoryRepo;

        public LicenseCategoryRepository(RepositoryContext repositoryContext, IRepositoryBase<License> _licenseRepo, IRepositoryBase<LicenseCategory> _licenseCategoryRepo)
            : base(repositoryContext)
        {
            licenseRepo = _licenseRepo;
            licenseCategoryRepo = _licenseCategoryRepo;
        }

        public LicenseCategory Get(int id, out string ErrMsg)
        {
            LicenseCategory result = new LicenseCategory();
            try
            {
                result = licenseCategoryRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public LicenseCategory GetActive(int id, out string ErrMsg)
        {
            LicenseCategory result = new LicenseCategory();
            try
            {
                result = licenseCategoryRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<LicenseCategory> GetActiveList(out string ErrMsg)
        {
            List<LicenseCategory> result = new List<LicenseCategory>();
            try
            {
                result = licenseCategoryRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<LicenseCategory> GetDeletedList(out string ErrMsg)
        {
            List<LicenseCategory> result = new List<LicenseCategory>();
            try
            {
                result = licenseCategoryRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(LicenseCategory licenseCategory, out string ErrMsg)
        {
            try
            {
                licenseCategoryRepo.Create(licenseCategory);
                licenseCategoryRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(LicenseCategory licenseCategory, out string ErrMsg)
        {
            try
            {
                RepositoryContext.Entry(licenseCategory).State = EntityState.Modified;
                licenseCategoryRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                LicenseCategory licenseCategory = licenseCategoryRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                licenseCategory.IsDeleted = true;
                licenseCategoryRepo.Update(licenseCategory);
                licenseCategoryRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                LicenseCategory licenseCategory = licenseCategoryRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                licenseCategoryRepo.Delete(licenseCategory);
                licenseCategoryRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                LicenseCategory licenseCategory = licenseCategoryRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                licenseCategory.IsDeleted = false;
                licenseCategoryRepo.Update(licenseCategory);
                licenseCategoryRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<LicenseCategory> licenseCategory = licenseCategoryRepo.GetByCondition(p => p.Name == name).ToList();

                if (isEdit == false && licenseCategory.Count > 0)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else if(isEdit == true && licenseCategory.Count > 1)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }
    }
}
