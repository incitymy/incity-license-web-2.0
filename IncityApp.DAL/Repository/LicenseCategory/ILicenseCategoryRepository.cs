﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface ILicenseCategoryRepository : IRepositoryBase<LicenseCategory>
    {
        LicenseCategory Get(int id, out string ErrMsg);
        LicenseCategory GetActive(int id, out string ErrMsg);
        List<LicenseCategory> GetActiveList(out string ErrMsg);
        List<LicenseCategory> GetDeletedList(out string ErrMsg);
        void Create(LicenseCategory licenseCategory, out string ErrMsg);
        void Edit(LicenseCategory licenseCategory, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, bool isEdit, out string ErrMsg);
    }
}
