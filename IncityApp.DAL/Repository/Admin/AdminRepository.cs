﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class AdminRepository : RepositoryBase<Admin>, IAdminRepository
    {
        private string selfName = "IncityApp.DAL.Repository.AdminRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "admin";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<AdminGroup> adminGroupRepo;
        private IRepositoryBase<Admin> adminRepo;

        public AdminRepository(RepositoryContext repositoryContext, IRepositoryBase<AdminGroup> _adminGroupRepo, IRepositoryBase<Admin> _adminRepo)
            : base(repositoryContext)
        {
            adminGroupRepo = _adminGroupRepo;
            adminRepo = _adminRepo;
        }

        public Admin Get(int id, out string ErrMsg)
        {
            Admin result = new Admin();
            try
            {
                result = adminRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public Admin GetActive(int id, out string ErrMsg)
        {
            Admin result = new Admin();
            try
            {
                result = adminRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Admin> GetActiveList(out string ErrMsg)
        {
            List<Admin> result = new List<Admin>();
            try
            {
                result = adminRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<Admin> GetDeletedList(out string ErrMsg)
        {
            List<Admin> result = new List<Admin>();
            try
            {
                result = adminRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(Admin admin, out string ErrMsg)
        {
            try
            {
                adminRepo.Create(admin);
                adminRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(Admin admin, out string ErrMsg)
        {
            try
            {
                adminRepo.Update(admin);
                adminRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                Admin admin = adminRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                admin.IsDeleted = true;
                adminRepo.Update(admin);
                adminRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                Admin admin = adminRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                adminRepo.Delete(admin);
                adminRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                Admin admin = adminRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                admin.IsDeleted = false;
                adminRepo.Update(admin);
                adminRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<Admin> admin = adminRepo.GetByCondition(p => p.Username == name).ToList();

                if (isEdit == false && admin.Count > 0)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else if (isEdit == true && admin.Count > 1)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public bool VerifyLogin(string username, string password, out string ErrMsg)
        {
            bool result = false;
            Admin admin = new Admin();
            try
            {
                admin = adminRepo.GetByCondition(p => p.Username == username && p.Password == password).FirstOrDefault();
                if (admin != null)
                {
                    if (admin.IsActive == true && admin.IsDeleted == false)
                    {
                        ErrMsg = "";
                        result = true;
                    }
                    else if (admin.IsDeleted == true)
                    {
                        ErrMsg = "Login failed. This account has been suspended.";
                    }
                    else if (admin.IsActive == false)
                    {
                        ErrMsg = "Login failed. This account has not activate.";
                    }
                    else
                    {
                        ErrMsg = "Login error. Please try again. If error persists, contact the software vendor.";
                    }
                }
                else
                {
                    ErrMsg = "Login failed. Incorrect username or password.";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }
    }
}
