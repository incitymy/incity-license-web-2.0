﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IAdminRepository : IRepositoryBase<Admin>
    {
        Admin Get(int id, out string ErrMsg);
        Admin GetActive(int id, out string ErrMsg);
        List<Admin> GetActiveList(out string ErrMsg);
        List<Admin> GetDeletedList(out string ErrMsg);
        void Create(Admin admin, out string ErrMsg);
        void Edit(Admin admin, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, bool isEdit, out string ErrMsg);
        bool VerifyLogin(string username, string password, out string ErrMsg);
    }
}
