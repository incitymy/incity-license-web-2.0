﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class OrderDetailRepository : RepositoryBase<OrderDetail>, IOrderDetailRepository
    {
        private string selfName = "IncityApp.DAL.Repository.OrderDetailRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "Order Detail ";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<Order> orderRepo;
        private IRepositoryBase<OrderDetail> orderDetailRepo;

        public OrderDetailRepository(RepositoryContext repositoryContext, IRepositoryBase<Order> _orderRepo, IRepositoryBase<OrderDetail> _orderDetailRepo)
            : base(repositoryContext)
        {
            orderRepo = _orderRepo;
            orderDetailRepo = _orderDetailRepo;
        }

        public OrderDetail Get(int id, out string ErrMsg)
        {
            OrderDetail result = new OrderDetail();
            try
            {
                result = orderDetailRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public OrderDetail GetActive(int id, out string ErrMsg)
        {
            OrderDetail result = new OrderDetail();
            try
            {
                result = orderDetailRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<OrderDetail> GetActiveList(out string ErrMsg)
        {
            List<OrderDetail> result = new List<OrderDetail>();
            try
            {
                result = orderDetailRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<OrderDetail> GetDeletedList(out string ErrMsg)
        {
            List<OrderDetail> result = new List<OrderDetail>();
            try
            {
                result = orderDetailRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(OrderDetail orderDetail, out string ErrMsg)
        {
            try
            {
                orderDetailRepo.Create(orderDetail);
                orderDetailRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(OrderDetail orderDetail, out string ErrMsg)
        {
            try
            {
                orderDetailRepo.Update(orderDetail);
                orderDetailRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                OrderDetail orderDetail = orderDetailRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                orderDetail.IsDeleted = true;
                orderDetailRepo.Update(orderDetail);
                orderDetailRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                OrderDetail orderDetail = orderDetailRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                orderDetailRepo.Delete(orderDetail);
                orderDetailRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                OrderDetail orderDetail = orderDetailRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                orderDetail.IsDeleted = false;
                orderDetailRepo.Update(orderDetail);
                orderDetailRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, int categoryId, bool isEdit, out string ErrMsg)
        {
            try
            {
                //List<OrderDetail> orderDetail = orderDetailRepo.GetByCondition(p => p.RefNo == name && p.OrderId == categoryId).ToList();

                //if (isEdit == false && orderDetail.Count > 0)
                //{
                //    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                //}
                //else if (isEdit == true && orderDetail.Count > 1)
                //{
                //    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                //}
                //else
                //{
                ErrMsg = "";
                //}

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        // API
        public void CheckLicenseCode(CheckLicenseModel checkLicenseModel, out ResponseModel response)
        {
            try
            {
                string msg = "1"; // Success
                string code = checkLicenseModel.LicenseCode.ToLower();
                string deviceId = checkLicenseModel.DeviceId;
                string altId = checkLicenseModel.DeviceAltId;
                string actDate = "";
                string expDate = "";

                List<OrderDetail> licenses = orderDetailRepo.GetByCondition(p => p.LicenseCode == code).ToList();

                if (licenses.Count() == 1)
                {
                    OrderDetail license = licenses.FirstOrDefault();

                    if (license.DeviceId == null || license.DeviceId == "")
                    {
                        license.DeviceId = deviceId;
                        orderDetailRepo.Update(license);
                        orderDetailRepo.Save();
                    }
                    else
                    {
                        if (license.DeviceId != deviceId)
                        {
                            msg = "4"; // DeviceId not match.
                        }
                    }

                    if (license.DeviceAltId == null || license.DeviceAltId == "")
                    {
                        license.DeviceAltId = altId;
                        orderDetailRepo.Update(license);
                        orderDetailRepo.Save();
                    }
                    else
                    {
                        if (license.DeviceAltId != altId)
                        {
                            msg = "5"; // DeviceAltId not match.
                        }
                    }

                    if (license.IsActive == false)
                    {
                        DateTime _date = new DateTime();
                        if(license.ActivateDate == _date)
                        {
                            license.ActivateDate = DateTime.Now;
                            license.ExpiredDate = DateTime.Now.AddDays(license.PeriodDay);
                            license.IsActive = true;

                            actDate = license.ActivateDate.ToString();
                            expDate = license.ExpiredDate.ToString();

                            orderDetailRepo.Update(license);
                            orderDetailRepo.Save();
                        }
                        else
                        {
                            actDate = license.ActivateDate.ToString();
                            expDate = license.ExpiredDate.ToString();

                            msg = "6"; // License already activated.
                        }
                    }
                    else
                    {
                        if(license.ExpiredDate < DateTime.Now)
                        {
                            actDate = license.ActivateDate.ToString();
                            expDate = license.ExpiredDate.ToString();

                            msg = "7"; // License already expired.
                        }
                    }
                }
                else if (licenses.Count() <= 0)
                {
                    msg = "2"; // License code does not exists.
                }
                else
                {
                    msg = "3"; // Duplicate license code.
                }
                
                var dictionary = new Dictionary<string, string> {{ "ActivationDate", actDate }, { "ExpiredDate", expDate }};
                response = new ResponseModel()
                {
                    ResponseMessage = msg,
                    ResponseObject = dictionary
                };
            }
            catch (Exception ex)
            {
                response = new ResponseModel()
                {
                    ResponseMessage = "An error has occured at " + selfName + "\n" + ex.Message.ToString()
                };
            }
        }
    }
}
