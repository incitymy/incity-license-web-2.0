﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IOrderDetailRepository : IRepositoryBase<OrderDetail>
    {
        OrderDetail Get(int id, out string ErrMsg);
        OrderDetail GetActive(int id, out string ErrMsg);
        List<OrderDetail> GetActiveList(out string ErrMsg);
        List<OrderDetail> GetDeletedList(out string ErrMsg);
        void Create(OrderDetail OrderDetail, out string ErrMsg);
        void Edit(OrderDetail OrderDetail, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, int categoryId, bool isEdit, out string ErrMsg);
        void CheckLicenseCode(CheckLicenseModel checkLicenseModel, out ResponseModel response);
    }
}
