﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public class AdminGroupRepository : RepositoryBase<AdminGroup>, IAdminGroupRepository
    {
        private string selfName = "IncityApp.DAL.Repository.AdminGroupRepository"; // This class's name, uses for ErrMsg to identifying which class has the error.
        private string className = "adminGroup ";

        //public void SampleMethod(out string ErrMsg)
        //{
        //    // Initiate
        //    try
        //    {
        //        // Method logics
        //        ErrMsg = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        // If failed
        //        ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
        //    }
        //}

        private IRepositoryBase<AdminGroup> adminGroupRepo;

        public AdminGroupRepository(RepositoryContext repositoryContext, IRepositoryBase<AdminGroup> _adminGroupRepo)
            : base(repositoryContext)
        {
            adminGroupRepo = _adminGroupRepo;
        }

        public AdminGroup Get(int id, out string ErrMsg)
        {
            AdminGroup result = new AdminGroup();
            try
            {
                result = adminGroupRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public AdminGroup GetActive(int id, out string ErrMsg)
        {
            AdminGroup result = new AdminGroup();
            try
            {
                result = adminGroupRepo.GetByCondition(p => p.IsActive && p.Id == id).FirstOrDefault();
                if (result == null)
                {
                    ErrMsg = "There are no active " + className + " with id " + id + ".";
                }
                else
                {
                    ErrMsg = "";
                }
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<AdminGroup> GetActiveList(out string ErrMsg)
        {
            List<AdminGroup> result = new List<AdminGroup>();
            try
            {
                result = adminGroupRepo.GetByCondition(p => p.IsActive == true && p.IsDeleted == false).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "There are no active " + className + ".";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public List<AdminGroup> GetDeletedList(out string ErrMsg)
        {
            List<AdminGroup> result = new List<AdminGroup>();
            try
            {
                result = adminGroupRepo.GetByCondition(p => p.IsDeleted == true).ToList();
                if (result == null || result.Count() <= 0)
                {
                    ErrMsg = "Trash list for " + className + " is empty.";
                }
                else
                {
                    ErrMsg = "";
                }
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
            return result;
        }

        public void Create(AdminGroup adminGroup, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.Create(adminGroup);
                adminGroupRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Edit(AdminGroup adminGroup, out string ErrMsg)
        {
            try
            {
                adminGroupRepo.Update(adminGroup);
                adminGroupRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Delete(int id, out string ErrMsg)
        {
            try
            {
                AdminGroup adminGroup = adminGroupRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                adminGroup.IsDeleted = true;
                adminGroupRepo.Update(adminGroup);
                adminGroupRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void DeletePermanant(int id, out string ErrMsg)
        {
            try
            {
                AdminGroup adminGroup = adminGroupRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                adminGroupRepo.Delete(adminGroup);
                adminGroupRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void Restore(int id, out string ErrMsg)
        {
            try
            {
                AdminGroup adminGroup = adminGroupRepo.GetByCondition(p => p.Id == id).FirstOrDefault();
                adminGroup.IsDeleted = false;
                adminGroupRepo.Update(adminGroup);
                adminGroupRepo.Save();
                ErrMsg = "";
            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }

        public void IsStringExists(string name, bool isEdit, out string ErrMsg)
        {
            try
            {
                List<AdminGroup> adminGroup = adminGroupRepo.GetByCondition(p => p.Name == name).ToList();

                if (isEdit == false && adminGroup.Count > 0)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else if (isEdit == true && adminGroup.Count > 1)
                {
                    ErrMsg = "Name already exists. If it was in your trash list, restore it instead.";
                }
                else
                {
                    ErrMsg = "";
                }

            }
            catch (Exception ex)
            {
                ErrMsg = "An error has occured at " + selfName + "\n" + ex.Message.ToString();
            }
        }
    }
}
