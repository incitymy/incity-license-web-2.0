﻿using System;
using System.Collections.Generic;
using System.Text;
using IncityApp.Models;

namespace IncityApp.DAL.Repository
{
    public interface IAdminGroupRepository : IRepositoryBase<AdminGroup>
    {
        AdminGroup Get(int id, out string ErrMsg);
        AdminGroup GetActive(int id, out string ErrMsg);
        List<AdminGroup> GetActiveList(out string ErrMsg);
        List<AdminGroup> GetDeletedList(out string ErrMsg);
        void Create(AdminGroup adminGroup, out string ErrMsg);
        void Edit(AdminGroup adminGroup, out string ErrMsg);
        void Delete(int id, out string ErrMsg);
        void DeletePermanant(int id, out string ErrMsg);
        void Restore(int id, out string ErrMsg);
        void IsStringExists(string name, bool isEdit, out string ErrMsg);
    }
}
