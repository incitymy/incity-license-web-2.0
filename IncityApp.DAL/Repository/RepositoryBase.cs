﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using IncityApp.Models;
using Microsoft.EntityFrameworkCore;

namespace IncityApp.DAL
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext { get; set; }

        public RepositoryBase(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public IEnumerable<T> GetAll()
        {
            return RepositoryContext.Set<T>().AsNoTracking(); // Use AsNoTracking() to detach loaded entities and free up space for update.
        }

        public IEnumerable<T> GetByCondition(Expression<Func<T, bool>> expression)
        {
            return RepositoryContext.Set<T>().AsNoTracking().Where(expression); // Use AsNoTracking() to detach loaded entities and free up space for update.
        }

        public void Create(T entity)
        {
            RepositoryContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            RepositoryContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            RepositoryContext.Set<T>().Remove(entity);
        }

        public void Save()
        {
            RepositoryContext.SaveChanges();
        }
    }
}
