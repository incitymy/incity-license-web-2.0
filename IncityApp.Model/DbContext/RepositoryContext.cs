﻿using System;
using Microsoft.EntityFrameworkCore;

namespace IncityApp.Models
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder
                //Log parameter values
                .EnableSensitiveDataLogging();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public DbSet<AdminLog> AdminLog { get; set; }
        public DbSet<AdminGroup> AdminGroup { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<License> License { get; set; }
        public DbSet<LicenseCategory> LicenseCategory { get; set; }
        public DbSet<Member> Customer { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
    }
}
