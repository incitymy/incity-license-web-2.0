﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using IncityApp.Models;

namespace IncityApp.Models
{
    public class HomeViewModel
    {
        public List<LicenseCategory> licenseCategories { get; set; }
        public List<License> licenses { get; set; }
    }
}
