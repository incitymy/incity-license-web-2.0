﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class CheckoutModel
    {
        public Cart cart { get; set; }
        public Member member { get; set; }
        public string PaymentMethod { get; set; }
    }
}
