﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class Cart
    {
        public List<CartItem> cartItems { get; set; }
        public decimal grandTotal { get; set; }
    }
}
