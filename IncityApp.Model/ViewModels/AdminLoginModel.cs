﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class AdminLoginModel
    {
        [Required(ErrorMessage = "Please enter your username.")]
        [MaxLength(100)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter your password.")]
        [MaxLength(100)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
