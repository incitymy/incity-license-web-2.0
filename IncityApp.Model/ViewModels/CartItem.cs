﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class CartItem
    {
        public License license { get; set; }
        public int qty { get; set; }
        public decimal subtotal { get; set; }
    }
}
