﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class ResponseModel
    {
        public string ResponseText { get; set; }
        public Object ResponseObject { get; set; }
        public List<Object> ResponseList { get; set; }
        public string ResponseMessage { get; set; }
    }
}
