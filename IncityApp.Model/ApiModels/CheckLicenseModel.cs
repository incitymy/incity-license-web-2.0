﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IncityApp.Models
{
    public class CheckLicenseModel
    {
        public string DeviceId { get; set; }
        public string DeviceAltId { get; set; }
        public string LicenseCode { get; set; }
    }
}
