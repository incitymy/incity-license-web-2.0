﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Models
{
    [Table("tblOrder")]
    public class Order : DbEntity
    {
        [Required]
        [Display(Name = "Ref No")]
        [MaxLength(100)]
        public string RefNo { get; set; }

        [Required]
        public int MemberId { get; set; }

        [Required]
        [Display(Name = "GrandTotal")]
        public decimal GrandTotal { get; set; }

        [Required]
        [Display(Name = "Payment Method")]
        [MaxLength(100)]
        public string PaymentMethod { get; set; }

        [Required]
        [Display(Name = "Status")]
        [MaxLength(100)]
        public string Status { get; set; }

        public List<OrderDetail> orderDetails { get; set; }

        [NotMapped]
        public SelectList StatusList { get; set; }
    }
}
