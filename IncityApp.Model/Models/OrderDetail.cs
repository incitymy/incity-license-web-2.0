﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IncityApp.Models
{
    [Table("tblOrder_Detail")]
    public class OrderDetail : DbEntity
    {
        [Required]
        public string LicenseCode { get; set; }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int LicenseId { get; set; }

        [Required]
        [Display(Name = "Qty")]
        public int Qty { get; set; }

        [Required]
        [Display(Name = "SubTotal")]
        public decimal SubTotal { get; set; }

        [Display(Name = "Activate Date")]
        public DateTime? ActivateDate { get; set; }

        [Display(Name = "Expired Date")]
        public DateTime? ExpiredDate { get; set; }

        [Required]
        [Display(Name = "Period Day")]
        public int PeriodDay { get; set; }

        public string DeviceId { get; set; }
        public string DeviceAltId { get; set; }

        public Order Order { get; set; }
        public License License { get; set; }
    }
}
