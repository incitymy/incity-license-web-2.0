﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IncityApp.Models
{
    [Table("tblMember")]
    public class Member : DbEntity
    {
        //[Display(Name = "Username")]
        //[MaxLength(100)]
        //public string Username { get; set; }

        [Required(ErrorMessage = "Full Name is required.")]
        [Display(Name = "Full Name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "E-mail address is required.")]
        [Display(Name = "E-mail Address")]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        [MaxLength(100)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Company name is required.")]
        [Display(Name = "Company Name")]
        [MaxLength(200)]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [Display(Name = "Phone No.")]
        [MaxLength(100)]
        public string PhoneNo { get; set; }

        [Display(Name = "Company Address")]
        [MaxLength(200)]
        public string Address { get; set; }

        [NotMapped]
        public string ReturnUrl { get; set; } // Will redirect to this url after registration if sated.
    }
}
