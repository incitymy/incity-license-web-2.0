﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IncityApp.Models
{
    [Table("tblLicense_category")]
    public class LicenseCategory : DbEntity
    {
        [Required(ErrorMessage = "License category name is required.")]
        [Display(Name = "Name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "License category description is required.")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public List<License> licenses { get; set; }
    }
}
