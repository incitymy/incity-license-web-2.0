﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Models
{
    [Table("tblLicense")]
    public class License : DbEntity
    {
        [Required(ErrorMessage = "License category is required.")]
        [ForeignKey("tblLicense_category")]
        [Display(Name = "Category")]
        public int LicenseCategoryId { get; set; }

        [Required(ErrorMessage = "License name is required.")]
        [Display(Name = "Name")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "License active period is required.")]
        [Display(Name = "Active Period")]
        public int PeriodDay { get; set; }

        [Required(ErrorMessage = "License price is required.")]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        public LicenseCategory LicenseCategory { get; set; }

        [NotMapped]
        public SelectList LicenseCategories { get; set; }
    }
}
