﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IncityApp.Models
{
    [Table("tblAdmin_Log")]
    public class AdminLog : DbEntity
    {
        [Display(Name = "Action")]
        public string Action { get; set; }

        [Display(Name = "Action Position")]
        public string ActionPosition { get; set; }

        [Display(Name = "IP Address")]
        public string IpAddress { get; set; }
    }
}
