﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IncityApp.Models
{
    [Table("tblAdmin")]
    public class Admin : DbEntity
    {
        [Required(ErrorMessage = "Username is required.")]
        [MaxLength(100)]
        public string Username { get; set; }

        [Required(ErrorMessage = "E-mail address is required.")]
        [MaxLength(100)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [MaxLength(100)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [Display(Name = "Phone No.")]
        [MaxLength(100)]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Admin group is required.")]
        [Display(Name = "Admin Group")]
        [ForeignKey("tblAdmin_Group")]
        public int AdminGroupId { get; set; }

        public AdminGroup AdminGroup { get; set; }

        [NotMapped]
        public SelectList AdminGroups { get; set; }
    }
}
