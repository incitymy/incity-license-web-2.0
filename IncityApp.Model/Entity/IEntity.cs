﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IncityApp.Models
{
    public interface IEntity
    {
        int Id { get; set; }
        //string Remark { get; set; }
        string CreateBy { get; set; }
        DateTime CreateDate { get; set; }
        string LastUser { get; set; }
        DateTime LastUpdate { get; set; }
    }
}
